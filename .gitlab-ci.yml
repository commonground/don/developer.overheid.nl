include:
  - project: commonground/don/ci/templates
    file: templates/base.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_DEPTH: 1
  # pull the latest commit of the submodule instead of the commit stored in the
  # submodule file in this repository
  GIT_SUBMODULE_UPDATE_FLAGS: --remote

# Lint and test:
#   image: python:3.11.6
#   stage: test
#   services:
#     - name: postgres:16.0-alpine
#       alias: postgres
#       variables:
#         POSTGRES_USER: don
#         POSTGRES_PASSWORD: don
#         POSTGRES_DB: don
#   variables:
#     PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
#     DB_HOST: postgres
#     DB_USER: don
#     DB_PASSWORD: don
#     DB_NAME: don
#     SECRET_KEY: somethingverysecret
#   before_script:
#     - pip3 install -r requirements-dev.txt
#   script:
#     - python3 manage.py makemigrations --check || (echo "Missing migrations"; false)
#     - python3 -m prospector
#     - python3 -m coverage run manage.py test
#     - python3 -m coverage report
#     - python3 -m coverage xml
#   cache:
#     paths:
#       - "$CI_PROJECT_DIR/.cache/pip"
#   coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
#   artifacts:
#     reports:
#       coverage_report:
#         coverage_format: cobertura
#         path: coverage.xml

Build:
  extends: .don_build_image

Deploy to review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    REVIEW_BASE_DOMAIN: nlx.reviews
    HELM_MAX_HISTORY: "1"
    HELM_KUBECONTEXT: commonground/don/ci/kubernetes-agents:review
    K8S_NAMESPACE: "don-${CI_ENVIRONMENT_SLUG}"
  before_script:
    - BUILD_IMAGE="`cat ci_build_image.txt`"
    - export IMAGE_REPOSITORY="${BUILD_IMAGE%:*}"
    - export IMAGE_TAG="${BUILD_IMAGE#*:}"
  script:
    - echo -e -n "https://don-${CI_ENVIRONMENT_SLUG}.${REVIEW_BASE_DOMAIN}" > ci_deploy_url.txt
    - kubectl --context "${HELM_KUBECONTEXT}" create namespace "${K8S_NAMESPACE}" || true
    - helm upgrade influxdb helm/influxdb --install --namespace ${K8S_NAMESPACE}
    - |
      helm upgrade don helm/don \
        --install \
        --namespace ${K8S_NAMESPACE} \
        --set-string "image.repository=${IMAGE_REPOSITORY}" \
        --set-string "image.tag=${IMAGE_TAG}" \
        --set-string "config.gitlabAccessToken=${GITLAB_ACCESS_TOKEN}" \
        --set-string "config.adminPassword=${TEST_ADMIN_PASSWORD}" \
        --set-string "ingress.host=${K8S_NAMESPACE}.${REVIEW_BASE_DOMAIN}" \
        --set-string "podAnnotations.app\.commonground\.nl/git-revision=${CI_COMMIT_SHORT_SHA}" \
        --values helm/don/values-review.yaml
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://${K8S_NAMESPACE}.${REVIEW_BASE_DOMAIN}
    on_stop: Remove from review
  artifacts:
    paths:
      - ci_deploy_url.txt
  dependencies:
    - Build
  needs:
    - Build
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'

Remove from review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    GIT_STRATEGY: none
    KUBE_CONTEXT: "commonground/don/ci/kubernetes-agents:review"
    K8S_NAMESPACE: "don-${CI_ENVIRONMENT_SLUG}"
  script:
    - kubectl --context "${KUBE_CONTEXT}" delete namespace "${K8S_NAMESPACE}"
  environment:
    name: $CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'
      when: manual
      allow_failure: true

Release:
  extends: .don_release_image
  dependencies:
    - Build
  needs:
    - Build

Deploy to production:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    HELM_KUBECONTEXT: "commonground/don/ci/kubernetes-agents:production"
    HELM_MAX_HISTORY: "2"
    K8S_NAMESPACE: tn-commonground-don
  before_script:
    - RELEASE_IMAGE="`cat ci_release_image.txt`"
    - IMAGE_REPOSITORY="${RELEASE_IMAGE%:*}"
    - IMAGE_TAG="${RELEASE_IMAGE#*:}"
  script:
    - echo -e -n "https://developer.overheid.nl" > ci_deploy_url.txt
    - |
      helm upgrade influxdb helm/influxdb \
        --install \
        --namespace "${K8S_NAMESPACE}" \
        --values helm/influxdb/values-production.yaml
    - |
      helm upgrade don helm/don \
        --install \
        --namespace "${K8S_NAMESPACE}" \
        --set-string image.repository=${IMAGE_REPOSITORY} \
        --set-string image.tag=${IMAGE_TAG} \
        --set-string config.gitlabAccessToken=${GITLAB_ACCESS_TOKEN} \
        --set-string config.githubAccessToken=${GITHB_ACCESS_TOKEN} \
        --set-string config.discourseApiKey=${DISCOURSE_API_KEY} \
        --values helm/don/values-production.yaml
  environment:
    name: production
    url: https://developer.overheid.nl
  artifacts:
    paths:
      - ci_deploy_url.txt
  dependencies:
    - Release
  needs:
    - Release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

Validate ADR:
  stage: integration
  image:
    name: registry.gitlab.com/commonground/don/adr-validator:0.5.0
    entrypoint: [""]
  before_script:
    - export DEPLOY_URL="`cat ci_deploy_url.txt`"
  script:
    - until wget -q --spider "${DEPLOY_URL}"; do sleep 3; done
    - adr-validator validate --details --fail-not-passed core "${DEPLOY_URL}/api/v0"
  dependencies:
    - Deploy to review
    - Deploy to production
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"

container_scanning:
  stage: security
  before_script:
    - export CS_IMAGE="`cat ci_build_image.txt`"
  dependencies:
    - Build
  needs:
    - Build

dependency_scanning:
  stage: security
  # needs:
  #   - Lint and test

gemnasium-python-dependency_scanning:
  variables:
    DS_IMAGE_SUFFIX: -python-3.10
  before_script:
    - apt update && apt install --yes libpq-dev

sast:
  stage: security
  # needs:
  #   - Lint and test
