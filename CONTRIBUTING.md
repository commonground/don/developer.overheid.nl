# Contributing

## Setup your development environment

### Requirements

* [Python](https://www.python.org/downloads/) >= 3.11. 
   * In unix operating systems you also need to install the python-venv package.
     For example on Ubuntu/Debian execute: `sudo apt install python3.11-venv`

> NOTE: Make sure that you install the right version of python. If you use a
> version lower than the one we recommend it will not work. After installing a
> new version of python your system might not directly use the right one. The
> easiest way to use the correct version of python is by making an alias in your
> .bashrc or .zshrc file: `alias python="python3.11"`

* [Node.js LTS](https://nodejs.org/en/download/)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install)
* [PostgreSQL 16](https://www.postgresql.org/download/)
   * Install Postgresql client libraries when you are not on Windows to be able to install
  `psycopg2`. Depending on the environment you have to do the following:
      * On Linux machine install postgresql client libs using the package
     manager. For example on Ubuntu/Debian execute `sudo apt install libpq-dev`
     and `python3.11-dev`
      * On Mac OS install postgresql, for example with Homebrew: `brew install
     postgresql`

### Setup

> Run all these commands within the project directory: `cd developer.overheid.nl`
> These commands you only need to run once when setting up the app for the first
> time.

Set up a Python virtual environment and install the dependencies.

```sh
python -m venv .venv
source .venv/bin/activate

# pip-tools is used to manage the dependencies
pip install pip-tools

# Install dependencies
pip-sync requirements-dev.txt
```

Start the database

> NOTE: make sure that the docker deamon is active. In unix: `sudo systemctl
> start docker`, in WSL `sudo dockerd`

```sh
docker compose up
```

Run the migrations

```sh
python manage.py migrate
```

Add yarn to your path with
[corepack](https://yarnpkg.com/getting-started/install) and install dependencies:

```sh
corepack enable
yarn install
```

### Run

> These commands you execute every time you want to run the application

Start a database for local development:

```sh
docker compose up
```

Run the django app

```sh
python manage.py runserver
```

Compile scss to CSS, and automatically recompile when there are changes to the scss

```sh
yarn watch
```

> NOTE: if there is an error in your css, yarn watch will crash and won't keep
> updating the CSS. You will have to fix the error, stop this process,
> and run yarn watch again.

### Lint your code

#### Manual

To lint your code from the command line with the same tool that is used in our
gitlab pipelines:

1. activate your virtual environment

```sh
source .venv/bin/activate
```

2. run prospector

```sh
prospector
```

3. if you have errors in a specific file, fix those errors, and then re-run
   prospector. Prospector is quite slow, so to save time you can run it on a
   specific file

```sh
prospector core/management/commands/sync_content.py
```

#### Automatic

To automatically lint your code, you can use pylint. In VS code you can use it
with the [pylint extension for VS
code](https://marketplace.visualstudio.com/items?itemName=ms-python.pylint).
For pylint to work well, you need to use the following settings:

```json
- "pylint.args": [ 
    "--load-plugins=pylint_django",
    "--django-settings-module=don.settings"
]
```

### Test your code

We test our code with unit tests. To run unit tests, run the database, activate
your virtual environment, and then:

```sh
python manage.py test
```

We don't have integration tests. Instead we have a simple bare metal htmx setup 🔥

## Sync content

To sync the list of API's and repositories from our
[content](https://gitlab.com/commonground/don/don-content) repository
into the local database:

1. Create a [gitlab access
   token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
   (with only `read_api` scope)
2. Copy your gitlab access token and set it in the
   `GITLAB_ACCESS_TOKEN` environment variable: `export
   GITLAB_ACCESS_TOKEN=glpat-Mj1v...`. If you don't want to keep this value in
   your command history start the command with a space.
3. Run the `sync_conent` command:

Often during development you will not need the complete list of API's and
repositories. That's why we have a few example API's and directories that we can
sync into the application during development:

```sh
python manage.py sync_content --directory test-content
```

If you want to sync all the content run:

```sh
python manage.py sync_content --repository https://gitlab.com/commonground/don/don-content
```

## Migrations

To make a new migration, create or edit a model in the python code, and then
run:

```sh
python manage.py makemigrations
```

To apply all migrations:

```sh
python manage.py migrate
```

To go back to a specific migration, find the number of the migration in
`core/migrations/` and then run the migrate command with a reference to that
specific migration. For example:

```sh
python manage.py migrate core 0044
```


## Run validator and generate reports

1. download an executable of the adr-validator:
   https://gitlab.com/commonground/don/adr-validator (or clone the repository
   and build the validator executable)
2. run the following command, with `--adr-validator-path` pointing to the
   location of the validator executable

```sh
python manage.py run_validators --adr-validator-path=../adr-validator
```
## Development

### Managing dependencies
#### Adding and updating dependencies

First, make sure your virtual environment is active.
#### New dependency

To include a new module dependency, add the module name to `requirements.in` (for production dependencies) or `requirements-dev.in` (for development and testing dependencies). Then update your dependencies.

#### Update dependencies

To update all dependencies to their newest versions, run

```sh
pip-compile requirements.in --generate-hashes --allow-unsafe
pip-compile requirements-dev.in --generate-hashes --allow-unsafe

pip-sync requirements-dev.txt
```

__*NB:*__ The order of the `pip-compile`'s is important!

If you updated your git repository and the new version has changes in the requirements files, also run the `pip-sync` command to synchronize your environment with the requirements files.

> __*Note*__: `pip-sync` will synchronize your environment with the requirements files, that means it will also delete any modules that are not listed in the requirements files. If you do not want that, run `pip install -r requirements-dev.txt` instead of `pip-sync`.

### Formatting HTML

HTML files can be formatted using this command:

```sh
djlint ./web/templates/partials/navigation_secondary_links.html --reformat
```

In the future, this can process can be made easier but for now a CLI tool will do.

## Static files

Under the `/static` url subpath we provide static files from our [static file
repository](https://gitlab.com/commonground/don/static). The way we do this is
by loading this repository as a submodule. To test these static files locally
all you need to do is run the following command for git to fetch the static
files:

```
git submodule update --remote
```

## Website analytics

We use [Goat Counter](https://www.goatcounter.com/) to measure visitor analytics on our website. We use Goat Counter because it's lightweight and privacy friendly. 

You can view the analytics on our website on this instance of Goat Counter: https://dongeitjes.commondatafactory.nl You can ask the team for the login details to our instance.

> NOTE: every time you visit https://developer.overheid.nl/ you also contribute to the analytics. To prevent this, always visit the website with `#toggle-goatcounter` at the end. The easiest way to do this consistently is by bookmarking the page: https://developer.overheid.nl/#toggle-goatcounter

## Conventions for commit messages

### Conventional commits

We follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. This convention requires you to add a type and an optional scope to your commit message. The scope is based on the applications in the repository. If you are not sure which scope to use please leave the scope blank.

The type must be one of the following:

* **build**: Changes that affect the build system or external dependencies
* **ci**: Changes to our CI configuration files and scripts
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **revert**: Changes that revert other changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests
* **content**: Changing copy on existing content pages

The available scopes are:

* helm

### Issue number prefix

For branches that are linked to a Gitlab issue, the commit message should also be prefixed by the issue number.  The issue number comes after the conventional commit part.

Example: `feat(ui): #235 replace add buttons with links on small devices`

