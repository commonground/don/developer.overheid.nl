import NumberOfOrgsLineChart from "./components/NumberOfOrgsLineChart";
import AdrBarChart from "./components/AdrBarChart";
import ApiSecLineChart from "./components/ApiSecLineChart";
import AdrLineChart from "./components/AdrLineChart";
import DashboardForm from "./components/DashboardForm";
import ApiTypeChart from "./components/ApiTypeChart";
import zoomPlugin from 'chartjs-plugin-zoom';
import ApiSecurityTypeChart from "./components/ApiSecurityTypeChart";
import ApiStatsChart from "./components/ApiStatsChart";
import ApiTypeOverTimeChart from "./components/ApiTypeOverTimeChart";


import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    BarElement,
    ArcElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
} from 'chart.js';
import ApiTypeOverTimeChart from "./components/ApiTypeOverTimeChart";


ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    BarElement,
    ArcElement,
    Title,
    Tooltip,
    Legend,
);

ChartJS.register(zoomPlugin);

export function DonApiStatsApp() {
    return <div>
        <div className="container mb-6">
            <DashboardForm />
        </div>
        <div class="container md:grid grid-cols-2 gap-4 mb-6">
            <NumberOfOrgsLineChart />
            <ApiStatsChart />
        </div>
        <div class="container flex flex-col">
            <div class="w-full">
                <h3 class="mb-1">Implementatie REST API Design Rules</h3>
            </div>
            <div class="w-full flex flex-col md:flex-row mb-6">
                <div class="md:w-1/3 flex justify-between flex-col grow mb-4 md:mb-0">
                    <p>Percentage toegepaste REST API Design Rules van het aantal API's. Omdat deze rules enkel gelden voor REST API's, kan de selectie van andere type API's een vertroebeld beeld geven.</p>
                    <AdrBarChart />
                </div>
                <div class="md:w-2/3 md:ml-4">
                    <AdrLineChart />
                </div>
            </div>
        </div>
        <div class="container flex flex-col mb-12">
            <div class="w-full">
                <h3 class="mb-1">API Types</h3>
            </div>
            <div class="w-full flex flex-col  md:flex-row mb-6">
                <div class="md:w-1/3 flex justify-between flex-col grow mb-4 md:mb-0">
                    <p>Totalen per type chronologisch (rechts) en op dit moment (onder).</p>
                    <ApiTypeChart />
                </div>
                <div class="md:w-2/3 md:ml-4">
                    <ApiTypeOverTimeChart />
                </div>
            </div>
        </div>
        <div class="container flex flex-col">
            <div class="w-full">
                <h3 class="mb-1">API Security</h3>
            </div>
            <div class="w-full flex flex-col md:flex-row mb-6">
                <div class="md:w-1/3 flex justify-between flex-col grow">
                    <p>Totalen per type beveiliging chronologisch (rechts) en op dit moment (onder).</p>
                    <ApiSecurityTypeChart />
                </div>
                <div class="md:w-2/3 md:ml-4">
                    <ApiSecLineChart />
                </div>
            </div>
        </div>
    </div >
}
