function constructUrl(searchParams, url) {
    const queryParameters = new URLSearchParams(window.location.search);
    for (const [key, value] of searchParams.entries()) {
        queryParameters.set(key, value);
    }
    queryParameters;
    return `/api/v0/stats/${url}?${queryParameters.toString()}`;
}

export async function fetchOrganizations(searchParams) {
    return fetch("/api/v0/organizations")
}

export async function fetchNumberApisOverTime(searchParams) {
    const url = constructUrl(searchParams, 'number-of-apis');
    return fetch(url)
}

export async function fetchOrgsOverTime(searchParams) {
    const url = constructUrl(searchParams, 'number-of-orgs');
    return fetch(url)
}

export async function fetchAdrData(searchParams) {
    const url = constructUrl(searchParams, 'api-design-rules');
    return fetch(url)
}

export async function fetchApiSecData(searchParams) {
    const url = constructUrl(searchParams, 'api-sec-data');
    return fetch(url)
}

export async function fetchApiTypeData(searchParams) {
    const url = constructUrl(searchParams, 'api-type-data');
    return fetch(url)
}