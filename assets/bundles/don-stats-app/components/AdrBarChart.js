import React from 'react';
import { Bar } from 'react-chartjs-2';
import { ADR_LABELS_SHORT, COLORS } from '../utils';

import { useStore } from '../store';

export default function () {

    const { state } = useStore();

    const options = {
        maintainAspectRatio: false,
        barThickness: 25,
        scales: {
            y: {
                max: 100,
                min: 0,
                ticks: {
                    stepSize: 10,
                    callback: function (value) {
                        return value + "%";
                    }
                }
            },
        },
        responsive: true,
        plugins: {
            legend: {
                display: false
            },
            tooltip: {
                callbacks: {
                    label: function (context) {
                        return context.parsed.y + '%';
                    },
                }
            },
        }
    }

    const dataValues = []
    const labels = []
    const backgroundColors = []
    const borderColors = []
    let i = 0;
    for (rule of Object.values(state.adr_data_over_time.rule_data)) {
        labels.push(rule.title);
        dataValues.push(rule.data[rule.data.length - 1]);
        backgroundColors.push(COLORS[i]);
        borderColors.push(COLORS[i]);
        i++;
    }

    const data = {
        labels,
        datasets: [
            {
                "label": "Rest API Design Rules",
                data: dataValues,
                minBarLength: 4,
                backgroundColor: backgroundColors,
                borderColor: borderColors,
            }
        ],
    };

    return (
        <div className='p-4 card'>
            <div className="h-64">
                <Bar data={data} options={options} />
            </div>
        </div>
    );
};
