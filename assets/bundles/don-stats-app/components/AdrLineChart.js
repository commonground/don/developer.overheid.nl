import React from 'react';
import { Line } from 'react-chartjs-2';
import { MONTHS_IN_DUTCH, COLORS, basicLineChartOptions } from '../utils';
import { useStore } from '../store';

export default function () {

    const { state } = useStore();

    const chartOptions = {
        ...basicLineChartOptions,
        plugins: {
            tooltip: {
                callbacks: {
                    label: function (context) {
                        return "" + context.parsed.y + "%";
                    },
                    title: function (context) {
                        return "API Design Rule: " + context[0].dataset.label;
                    }
                }
            },
        },
        responsive: true,
        scales: {
            y: {
                ticks: {
                    stepSize: 10,
                    callback: function (value) {
                        return value + "%";
                    }
                }
            },
        },
    };

    const datasets = []
    let i = 0;
    for (rule of Object.values(state.adr_data_over_time.rule_data)) {
        datasets.push({
            "label": rule.title,
            "data": rule.data,
            "borderColor": COLORS[i],
            "backgroundColor": COLORS[i],
            "cubicInterpolationMode": 'monotone',
        });
        i++;
    }


    const data = {
        labels: state.adr_data_over_time.labels,
        datasets,
    };

    return (
        <div>
            <div className='card p-4'>
                <Line data={data} options={chartOptions} />
            </div>
        </div>
    );
};
