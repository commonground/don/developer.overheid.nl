import React from 'react';
import { Line } from 'react-chartjs-2';
import { MONTHS_IN_DUTCH, COLORS, basicLineChartOptions, basicLineChartOptions } from '../utils';
import { useStore } from '../store';

function ApiSecLineChart() {

    const { state } = useStore();

    const chartOptions = {
        ...basicLineChartOptions,
        plugins: {
            tooltip: {
                callbacks: {
                    label: function (context) {
                        return "Aantal: " + context.parsed.y;
                    },
                    title: function (context) {
                        return "API security type: " + context[0].dataset.label;
                    }
                }
            },
        },
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    precision: 0,
                }
            },
        },
    };

    const datasets = []
    let i = 0;
    for (apiSecType of Object.entries(state.api_sec_data.data)) {
        datasets.push({
            label: apiSecType[0],
            data: apiSecType[1],
            cubicInterpolationMode: 'monotone',
            borderColor: COLORS[i],
            backgroundColor: COLORS[i],
        });
        i++;
    }

    const data = {
        labels: state.api_sec_data.labels,
        datasets,
    };

    return (
        <div>
            <div className='card p-4'>
                <Line data={data} options={chartOptions} />
            </div>
        </div>
    );
};

export default ApiSecLineChart;
