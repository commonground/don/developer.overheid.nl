import React from 'react'
import { Doughnut } from 'react-chartjs-2';
import { COLORS, showPercentagesTooltipLabel } from '../utils';
import { useStore } from '../store';

function ApiSecurityTypeChart() {

    const { state } = useStore();

    const chartOptions = {
        plugins: {
            legend: {
                position: 'left'
            },
            tooltip: {
                callbacks: {
                    label: showPercentagesTooltipLabel
                }
            },
        },
        layout: {
            padding: {
                left: -10
            }
        },
    }

    const datasetData = []
    const labels = []
    let i = 0;
    for (const apiSecType of Object.entries(state.api_sec_data.data)) {
        labels.push(apiSecType[0])
        const data = apiSecType[1]
        datasetData.push(data[data.length - 1])
    }

    const data = {
        labels,
        datasets: [
            {
                label: "Aantal API's",
                data: datasetData,
                backgroundColor: COLORS,
            },
        ]
    };

    return (
        <div className='card relative'>
            <div class="h-72 top-0 p-2">
                <div class="mt-[-46px]">
                    <Doughnut data={data} options={chartOptions} />
                </div>
            </div>
        </div>
    );
};

export default ApiSecurityTypeChart;
