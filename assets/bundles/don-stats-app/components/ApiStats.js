import ApiStatsChart from './ApiStatsChart';
import ApiTypeChart from './ApiTypeChart';
import ApiTypeOverTimeChart from './ApiTypeOverTimeChart';
import AdrImplementationForOrgChart from './AdrImplementationForOrgChart';
import ApiSecurityTypeOverTimeChart from './ApiSecurityTypeOverTimeChart';
import AdrImplementationOverTimeForOrgChart from './AdrImplementationOverTimeForOrgChart';
import OrgPeriodSelector from './OrgPeriodSelector';

function ApiStats() {
    return (
        <div>
            <div className="mb-6 grid grid-cols-2 gap-4">
                <div>
                    <h3>Aantal API's</h3>
                    <div className="card p-4">
                        <ApiStatsChart></ApiStatsChart>
                    </div>
                </div>
                <div>
                    <h3>Implementatie API Design Rules</h3>
                    <div className="card p-4">
                        <AdrImplementationForOrgChart></AdrImplementationForOrgChart>
                    </div>
                </div>
                <div className='mb-6'>
                    <h3>Type API's</h3>
                    <div className="card p-4">
                        <ApiTypeChart></ApiTypeChart>
                    </div>
                </div>
                <div>
                    <h3>Type security API's</h3>
                    <div className="card p-4">

                    </div>
                </div>
            </div>
            <OrgPeriodSelector />
            <div className='mb-6'>
                <h3>Implementatie API Design Rules over tijd</h3>
                <div className="card p-4">
                    <AdrImplementationOverTimeForOrgChart />
                </div>
            </div>
            <div className="mb-6">
                <div className='mb-6'>
                    <h3>Type API's over tijd</h3>
                    <div className="card p-4">
                        <ApiTypeOverTimeChart />
                    </div>
                </div>
            </div>
            <div>
                <h3>Type security API's over tijd</h3>
                <div className="card p-4 mb-6">
                    <ApiSecurityTypeOverTimeChart />
                </div>
            </div>
        </div>
    );
};

export default ApiStats
