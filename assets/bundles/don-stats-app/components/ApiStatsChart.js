import React from 'react';
import { Line } from 'react-chartjs-2';
import { useStore } from '../store';
import { basicLineChartOptions } from '../utils';

export default function () {

    const { state } = useStore();

    const chartOptions = {
        ...basicLineChartOptions,
        plugins: {
            legend: {
                display: false
            }
        },
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    precision: 0,
                }
            },
        },
    }

    const data = {
        labels: state.api_totals.labels,
        datasets: [
            {
                label: "Aantal API's",
                data: state.api_totals.data,
                borderColor: 'rgb(75, 192, 192)',
                "cubicInterpolationMode": 'monotone',
            }
        ]
    };

    return (
        <div>
            <h3>Aantal API's</h3>
            <div class="card p-2 md:p-4">
                <Line data={data} options={chartOptions} />
            </div>
        </div>
    );
};
