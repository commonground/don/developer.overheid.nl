

import React from 'react';
import { Doughnut } from "react-chartjs-2";
import { COLORS, showPercentagesTooltipLabel } from "../utils";
import { useStore } from "../store";

function ApiTypeChart() {

    const { state } = useStore();

    const chartOptions = {
        plugins: {
            legend: {
                position: 'left'
            },
            tooltip: {
                callbacks: {
                    label: showPercentagesTooltipLabel
                }
            },
        },
        layout: {
            padding: {
                left: -10,
            }
        },
    }

    const dataValues = []
    const backgroundColors = []
    const labels = []
    let i = 0;
    for (apiTypeData of Object.values(state.api_type_data.data)) {
        labels.push(apiTypeData.title);
        dataValues.push(apiTypeData.data[apiTypeData.data.length - 1]);
        backgroundColors.push(COLORS[i]);
        i++;
    }

    const data = {
        labels,
        datasets: [
            {
                label: "Aantal API's",
                data: dataValues,
                backgroundColor: backgroundColors,
            },
        ]
    };

    return (
        <div className='card relative'>
            <div class="h-72 top-0 p-2">
                <div class="mt-[-46px]">
                    <Doughnut data={data} options={chartOptions} />
                </div>
            </div>
        </div>
    )
};


export default ApiTypeChart
