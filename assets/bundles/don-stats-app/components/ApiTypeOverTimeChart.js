

import React from 'react';
import { basicLineChartOptions, COLORS, basicLineChartOptions } from '../utils';
import { Line } from 'react-chartjs-2';
import { useStore } from '../store';



function ApiTypeOverTimeChart() {

    const { state } = useStore();
    let i = 0;
    const datasets = []

    for (apiTypeData of Object.values(state.api_type_data.data)) {
        datasets.push({
            label: apiTypeData.title,
            data: apiTypeData.data,
            backgroundColor: COLORS[i],
            borderColor: COLORS[i],
            "cubicInterpolationMode": "monotone",
        });
        i++;
    }

    const data = {
        labels: state.api_type_data.labels,
        datasets
    };

    const chartOptions = {
        ...basicLineChartOptions,
        responsive: true,
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    stepSize: 1,
                }
            },
        },
    }

    return (
        <div class="card p-4">
            <Line data={data} options={chartOptions} />
        </div>
    )
};


export default ApiTypeOverTimeChart
