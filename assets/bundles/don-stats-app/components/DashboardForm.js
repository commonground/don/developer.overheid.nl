import Select from 'react-select'
import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import { SELECT_TIMESPAN_OPTIONS, API_SEC_TYPES, API_TYPES } from '../utils';
import { useStore } from '../store';
import { fetchAndSetNumberApis, fetchAndSetOrganizations, fetchAndSetOrgStats, fetchAndSetAdrData, fetchAndSetApiSecData, fetchAndSetApiTypeData } from '../services/dashboardService';
import { log } from 'util';

export default function DashboardForm() {

    const { state, dispatch } = useStore();

    const [searchParams, setSearchParams] = useSearchParams();
    const apiSecFormRef = React.useRef(null);
    const apiTypeFormRef = React.useRef(null);

    useEffect(() => { // Called after component mounted
        setMonthField(searchParams);
        setApiTypeFields();
        setApiSecFields();
        updateOverallData();

        // Fetch data for filling the org picker dropdown
        fetchAndSetOrganizations(dispatch, searchParams);
    }, []);

    const isApiTypeChecked = (apiType) => {
        const queryParameters = new URLSearchParams(window.location.search)
        const orgs = queryParameters.get("api_type")
        if (!orgs) {
            return false;
        }
        return orgs.includes(apiType);
    }

    isApiSecChecked = (apiSecType) => {
        const queryParameters = new URLSearchParams(window.location.search)
        const orgs = queryParameters.get("api_sec")
        if (!orgs) {
            return false;
        }
        return orgs.includes(apiSecType);
    }

    const setMonthField = (searchParams) => {
        if (!searchParams.has('months')) {
            searchParams.set('months', 12);
            setSearchParams(searchParams);
        }
    }

    const setApiTypeFields = () => {
        if (searchParams.has('api_type')) {
            const apiTypes = searchParams.get('api_type').split('|');
            dispatch({ type: 'SET_API_TYPE_FILTER', payload: apiTypes });
        }
    }

    const setApiSecFields = () => {
        if (searchParams.has('api_sec')) {
            const apiSecTypes = searchParams.get('api_sec').split('|');
            dispatch({ type: 'SET_API_SEC_FILTER', payload: apiSecTypes });
        }
    }
    const updateOverallData = async () => {
        fetchAndSetNumberApis(dispatch, searchParams);
        fetchAndSetOrgStats(dispatch, searchParams);
        fetchAndSetAdrData(dispatch, searchParams);
        fetchAndSetApiSecData(dispatch, searchParams);
        fetchAndSetApiTypeData(dispatch, searchParams);
    }

    const organizationChanged = async (event) => {
        if (event.slug === '') {
            removeParam("org");
        } else {
            searchParams.set('org', event.value);
            setSearchParams(searchParams)
        }

        updateOverallData();
    }

    const removeParam = (paramSlug) => {
        searchParams.delete(paramSlug);
        setSearchParams(searchParams);
    };

    const monthsChanged = async (event) => {
        searchParams.set('months', event.value);
        setSearchParams(searchParams)

        updateOverallData();
    }

    const apiTypeChange = async () => {
        let checkedFields = [];
        const formElements = apiTypeFormRef.current.children;

        for (let i = 0; i < formElements.length; i++) {
            const inputElement = formElements[i].querySelector('input');
            if (inputElement && inputElement.checked) {
                checkedFields.push(inputElement.value);
            }
        }

        if (checkedFields.length === 0) {
            searchParams.delete('api_type');
            dispatch({ type: 'SET_API_TYPE_FILTER', payload: [] });
        } else {
            dispatch({ type: 'SET_API_TYPE_FILTER', payload: checkedFields });
            searchParams.set('api_type', checkedFields.join('|'));
        }

        setSearchParams(searchParams);
        updateOverallData();
    }

    const apiSecChange = async () => {
        let checkedFields = [];
        const formElements = apiSecFormRef.current.children;

        for (let i = 0; i < formElements.length; i++) {
            const inputElement = formElements[i].querySelector('input');
            if (inputElement && inputElement.checked) {
                checkedFields.push(inputElement.value);
            }
        }

        if (checkedFields.length === 0) {
            searchParams.delete('api_sec');
            dispatch({ type: 'SET_API_SEC_FILTER', payload: [] });
        } else {
            dispatch({ type: 'SET_API_SEC_FILTER', payload: checkedFields });
            searchParams.set('api_sec', checkedFields.join('|'));
        }

        setSearchParams(searchParams);
        updateOverallData();
    }

    // Helper function to find the option object
    const findMonthsOption = (options, value) => options.find(option => option.value === value);

    const ApiTypeFields = API_TYPES.map(apiType => {
        return <span className='checkbox-list__option'>
            <input type="checkbox" id={`apiSec-${apiType.value}`} checked={isApiTypeChecked(apiType.value)} name={apiType.label} value={apiType.value} />
            <label htmlFor={`apiSec-${apiType.value}`}>{apiType.label}</label>
        </span>;
    });

    const apiSecurityTypeFields = API_SEC_TYPES.map(apiSecurityType => {
        return <span className='checkbox-list__option'>
            <input type="checkbox" id={apiSecurityType.value} checked={isApiSecChecked(apiSecurityType.value)} name={apiSecurityType.label} value={apiSecurityType.value} />
            <label htmlFor={apiSecurityType.value}>{apiSecurityType.label}</label>
        </span>;
    });

    const currentOrg = state.organizations.find(org => Number(org.value) === Number(searchParams.get('org')));

    return <div className='api-dashboard-form w-100 bg-white card p-4 md:w-64 mt-[29px]'>
        <div className="mb-4">
            <h3 className='text-sm'>Organisatie</h3>
            <Select
                value={currentOrg}
                options={state.organizations}
                onChange={organizationChanged} />
        </div>
        <div class="mb-4 md:mb-0">
            <h3 className='text-sm'>Tijdsduur</h3>
            <Select
                onChange={monthsChanged}
                value={findMonthsOption(SELECT_TIMESPAN_OPTIONS, searchParams.get('months')) || SELECT_TIMESPAN_OPTIONS[0]}
                isSearchable={false}
                options={SELECT_TIMESPAN_OPTIONS} />
        </div>
        <div class="mb-4 md:mb-0">
            <h3 className='text-sm'>API Type</h3>
            <form ref={apiTypeFormRef} className="checkbox-list mt-0 mb-0 grid grid-cols-2" onChange={apiTypeChange}>
                {ApiTypeFields}
            </form>
        </div>
        <div>
            <h3 className='text-sm'>API Security</h3>
            <form ref={apiSecFormRef} className="checkbox-list mt-0 mb-0  grid grid-cols-2" onChange={apiSecChange}>
                {apiSecurityTypeFields}
            </form>
        </div>
    </div>
}
