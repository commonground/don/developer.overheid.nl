import React from 'react';
import { useStore } from '../store';
import { Line } from 'react-chartjs-2';
import { basicLineChartOptions } from '../utils';


function NumberOfOrgsLineChart() {

    const { state } = useStore();

    const data = {
        labels: state.org_totals.labels,
        datasets: [
            {
                label: 'Aantal organisaties',
                data: state.org_totals.data,
                borderColor: 'rgb(75, 192, 192)',
                cubicInterpolationMode: 'monotone',
            }
        ]
    };

    const chartOptions = {
        ...basicLineChartOptions,
        scales: {
            y: {
                beginAtZero: true,
                ticks: {
                    precision: 0,
                }
            },
        },
        plugins: {
            legend: {
                display: false,
            }
        },
    }

    return (
        <div>
            <h3>Aantal organisaties</h3>
            <div className="card p-2 md:p-4 mb-4 md:mb-0">
                <Line data={data} options={chartOptions} />
            </div>
        </div>
    );
};

export default NumberOfOrgsLineChart;

