import { createRoot } from "react-dom/client";
import { DonApiStatsApp } from "./DonApiStatsApp";

import { StoreProvider } from "./store";
import { BrowserRouter } from 'react-router-dom';

const container = document.getElementById("don-stats-app");
const root = createRoot(container)
root.render(
    <StoreProvider>
        <BrowserRouter>
            <DonApiStatsApp />
        </BrowserRouter>
    </StoreProvider>
);
