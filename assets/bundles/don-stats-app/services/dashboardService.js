// src/services/dashboardService.js
import { fetchOrganizations, fetchNumberApisOverTime, fetchOrgsOverTime, fetchAdrData, fetchApiSecData, fetchApiTypeData } from "../api";

export async function fetchAndSetOrganizations(dispatch) {
    const response = await fetchOrganizations();
    const data = await response.json();
    const sortedOrgs = data.sort((a, b) => a.name.localeCompare(b.name));
    const mappedOrgs = sortedOrgs.map(org => {
        return {
            value: org.id,
            label: org.name,
        };
    });
    const orgFields = [
        { value: '', label: 'Alle organisaties' },
        ...mappedOrgs,
    ];
    dispatch({ type: 'SET_ORGANIZATIONS', payload: orgFields });
}

export async function fetchAndSetNumberApis(dispatch, searchParams) {
    const response = await fetchNumberApisOverTime(searchParams);
    const data = await response.json();
    dispatch({ type: 'SET_API_TOTALS', payload: data });
}

export async function fetchAndSetOrgStats(dispatch, searchParams) {
    const response = await fetchOrgsOverTime(searchParams);
    const data = await response.json();
    dispatch({ type: 'SET_ORG_TOTALS', payload: data });
}

export async function fetchAndSetAdrData(dispatch, searchParams) {
    const response = await fetchAdrData(searchParams)
    const data = await response.json();
    dispatch({ type: 'SET_ADR_DATA_OVER_TIME', payload: data });
}

export async function fetchAndSetApiSecData(dispatch, searchParams) {
    const response = await fetchApiSecData(searchParams)
    const data = await response.json();
    dispatch({ type: 'SET_API_SEC_DATA', payload: data });
}

export async function fetchAndSetApiTypeData(dispatch, searchParams) {
    const response = await fetchApiTypeData(searchParams)
    const data = await response.json();
    dispatch({ type: 'SET_API_TYPE_DATA', payload: data });
}