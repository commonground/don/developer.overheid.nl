// store.js
import React, { createContext, useReducer, useContext } from 'react';

// Initial state
const initialState = {
    api_totals: {
        "labels": [],
        "data": [],
    },
    org_totals: {
        "labels": [],
        "data": [],
    },
    organizations: [],
    adr_data_over_time: {
        "rule_data": {},
        "labels": [],
    },
    api_sec_data: {
        "labels": [],
        "data": [],
    },
    api_type_data: {
        "labels": [],
        "data": [],
    },
    api_type_filter: [],
    api_sec_filter: [],
    selected_org: {},
};

// Reducer function
function reducer(state, action) {
    switch (action.type) {
        case 'SET_API_TOTALS':
            return {
                ...state,
                api_totals: action.payload,
            };
        case 'SET_ORG_TOTALS':
            return {
                ...state,
                org_totals: action.payload,
            };
        case 'SET_ORGANIZATIONS':
            return {
                ...state,
                organizations: action.payload,
            };
        case 'SET_ADR_DATA_OVER_TIME':
            return {
                ...state,
                adr_data_over_time: action.payload,
            };
        case 'SET_API_SEC_DATA':
            return {
                ...state,
                api_sec_data: action.payload,
            };
        case 'SET_API_TYPE_DATA':
            return {
                ...state,
                api_type_data: action.payload,
            };
        default:
            return state;
    }
}

// Create context
const StoreContext = createContext();

// Store provider component
export function StoreProvider({ children }) {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <StoreContext.Provider value={{ state, dispatch }}>
            {children}
        </StoreContext.Provider>
    );
}

// Custom hook to use the store
export function useStore() {
    return useContext(StoreContext);
}
