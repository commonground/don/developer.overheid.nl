
import colors from "tailwindcss/colors";

export const MONTHS_IN_DUTCH = [
    'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
    'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'
];

export const ADR_LABELS_SHORT = [
    "/core/no-trailing-slash",
    "/core/http-methods",
    "/core/doc-openapi",
    "/core/publish-openapi",
    "/core/uri-version",
    "/core/semver",
    "/core/version-header",
    // "/core/transport-security"
]

export const basicLineChartOptions = {
    interaction: {
        intersect: false,
        mode: 'nearest'
    },
}

export function getRandomPercentages(numberOfValues) {
    const percentages = [];
    for (let i = 0; i < numberOfValues; i++) {
        percentages.push(Math.floor(Math.random() * 101));
    }
    return percentages;
}

export const ADR_LABELS_LONG = [
    "Leave off trailing slashes from URIs",
    "Only apply standard HTTP methods",
    "Use OpenAPI Specification for documentation",
    "Publish OAS document at a standard location in JSON-format",
    "Include the major version number in the URI",
    "Adhere to the Semantic Versioning model when releasing API changes",
    "Return the full version number in a response header",
    // "Apply the transport security module"
]

export const API_TYPES = [
    { value: 'odata', label: 'OData' },
    { value: 'rest_json', label: 'REST/JSON' },
    { value: 'rest_xml', label: 'REST/XML' },
    { value: 'graphql', label: 'GraphQL' },
    { value: 'wfs', label: 'WFS' },
    { value: 'wms', label: 'WMS' },
    { value: 'unknown', label: 'Onbekend' },
]

export const API_SEC_TYPES = [
    { value: 'none', label: 'Geen' },
    { value: 'unknown', label: 'Onbekend' },
    { value: 'api_key', label: 'API key' },
    { value: 'oauth2', label: 'OAuth 2.0' },
]


export function getRandomValues(length) {
    return Array.from({ length }, () => Math.floor(Math.random() * 40));
}

export function getRandomPercentage() {
    return Math.floor(Math.random() * 100)
}

export function getRandomValueBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


export function showPercentagesTooltipLabel(context) {
    const sum = context.dataset.data.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
    const percentage = sum > 0 ? Math.round(context.parsed / sum * 100) : 0;

    const label = ["Aantal: " + context.parsed];
    label.push("Percentage: " + percentage + "%");
    return label
}

export const COLORS = [
    colors.emerald[400],
    colors.red[400],
    colors.sky[400],
    colors.pink[400],
    colors.indigo[500],
    colors.amber[400],
    colors.lime[400],
    colors.rose[400],
    colors.orange[400]
]

export const COLOR_INTENSITY = 400;

export const ADR_RULES = [
    'API-03',
    'API-16',
    'API-20',
    'API-48',
    'API-51',
    'API-56',
    'API-57'
];

export const ADR_RULES_DESCRIPTION = [
    'API-03: Only apply standard HTTP methods',
    'API-16: Use OpenAPI Specification for documentation',
    'API-20: Include the major version number in the URI',
    'API-48: Leave off trailing slashes from URIs',
    'API-51: Publish OAS document at a standard location in JSON-format',
    'API-56: Adhere to the Semantic Versioning model when releasing API changes',
    'API-57: Return the full version number in a response header'
];

export const SELECT_TIMESPAN_OPTIONS = [
    { value: '3', label: '3 maanden' },
    { value: '6', label: '6 maanden' },
    { value: '12', label: '1 jaar' },
]
