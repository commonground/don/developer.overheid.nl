from typing import Iterable, Tuple

from django.db import transaction

from core.models.api import API, Environment, APIValidatorReport
from core.validators.base import Validator


def get_apis_to_validate() -> Iterable[Tuple[API, Environment]]:
    apis: list[API] = API.objects.filter(api_type__in=API.ADR_TESTABLE_TYPES)

    for api in apis:
        env: Environment = api.get_production_environment()
        if not env:
            continue

        yield api, env


@transaction.atomic
def validate_api(validator: Validator, api: API, uri: str):

    # coreReport = validator.validate_ruleset("core", uri)
    # APIValidatorReport.objects.create(report=coreReport, api=api)

    coreReport = validator.validate_ruleset("core-v2", uri)
    APIValidatorReport.objects.create(report=coreReport, api=api)

    securityReport = validator.validate_ruleset("security-tls", uri)
    APIValidatorReport.objects.create(report=securityReport, api=api)
