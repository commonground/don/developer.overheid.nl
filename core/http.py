from requests import Session, Response
from requests.adapters import HTTPAdapter


MAX_RETRIES = 2
REQUEST_TIMEOUT = 5
USER_AGENT = "DON/0.1.0 (Developer Overheid; +https://developer.overheid.nl)"


class DONSession(Session):
    def __init__(self, base_url: str):
        super().__init__()

        self._base_url = base_url
        self.headers["User-Agent"] = USER_AGENT

        self.mount("https://", HTTPAdapter(max_retries=MAX_RETRIES))
        self.mount("http://", HTTPAdapter(max_retries=MAX_RETRIES))

    def request(self, method: str, url: str, *args, **kwargs) -> Response:
        url = f"{self._base_url}{url}"
        kwargs.setdefault("timeout", REQUEST_TIMEOUT)
        return super().request(method, url, *args, **kwargs)


def create_http_client(base_url: str = "") -> Session:
    return DONSession(base_url)


default_client = create_http_client()
