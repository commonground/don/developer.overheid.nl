from django.conf import settings
from django.core.management.base import BaseCommand

from core.linkchecker import get_urls_to_check, check_urls, find_new_broken_urls, report_urls, LinkcheckerException
from core.repository import create_gitlab_client


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--failure-threshold", default=settings.LINKCHECKER_FAILURE_THRESHOLD, type=int)
        parser.add_argument(
            "--project-id", default=settings.GITLAB['PROJECT_ID'], type=str)

    # pylint:disable=arguments-differ
    def handle(self, failure_threshold: int, project_id: int, *args, **options):
        gitlab_client = create_gitlab_client()
        urls = get_urls_to_check()

        self.stdout.write(f"Checking {len(urls)} URL's...")
        check_urls(urls)

        self.stdout.write("Finding new broken URL's...")
        broken_urls = find_new_broken_urls(urls, failure_threshold)

        self.stdout.write(f"New broken URL's found: {len(broken_urls)}")

        if not broken_urls:
            self.stdout.write("Nothing to report")
            return

        self.stdout.write("Reporting broken URL's...")
        try:
            issue = report_urls(gitlab_client, project_id, broken_urls)
            self.stdout.write(f"Created issue: {issue.web_url}")
        except LinkcheckerException as e:
            self.stderr.write(f"Failed to create issue: {e}")
