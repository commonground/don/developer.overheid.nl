import sys

from django.conf import settings
from django.core.management.base import BaseCommand

from core.models.api import API, APIValidatorReport
from core.models.repository import Repository, RepositoryValidatorReport
from core.adr import get_apis_to_validate, validate_api
from core.validators import validators
from core.validators.base import ValidatorExecutionError


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--adr-validator-path", default=settings.ADR_VALIDATOR_PATH)

    def handle(self, *args, **options):
        # NOTE: repository validator deactivated until we make use of the results. When activating, make sure to also
        # fetch and store the readme_url in core/repository.py
        # if self._validate_api_metadata() or self._validate_adr() or self._validate_repository_metadata():
        if self._validate_api_metadata() or self._validate_adr():
            sys.exit(1)

    def _validate_api_metadata(self):
        self.stdout.write("Running API metadata validator")

        validator = validators["api-metadata"]
        has_failures = False

        for api in API.objects.all():
            self.stdout.write(f"Validating API: {api}")
            try:
                report = validator.validate_ruleset("metadata", api.api_id)
                APIValidatorReport.objects.create(report=report, api=api)
            except Exception as error:
                has_failures = True
                self.stderr.write(f"Unexptected error: {error}", self.style.ERROR)

        return has_failures

    def _validate_adr(self):
        self.stdout.write("Running ADR validator")

        adr_validator = validators["adr"]
        has_failures = False

        for api, env in get_apis_to_validate():
            self.stdout.write(f"Validating API: {api} ({env.api_url})")
            try:
                validate_api(adr_validator, api, env.api_url)
            except ValidatorExecutionError as error:
                self.stdout.write(f"Validation failed: {error}", self.style.ERROR)
            except Exception as error:
                has_failures = True
                self.stderr.write(f"Unexptected error: {error}", self.style.ERROR)

        return has_failures

    def _validate_repository_metadata(self):
        self.stdout.write("Running repository metadata validator")

        validator = validators["repository"]
        has_failures = False

        for repository in Repository.objects.all():
            self.stdout.write(f"Validating repository: {repository}")
            try:
                report = validator.validate_ruleset("metadata", repository.id)
                RepositoryValidatorReport.objects.create(report=report, repository=repository)
            except Exception as error:
                has_failures = True
                self.stderr.write(f"Unexptected error: {error}", self.style.ERROR)

        return has_failures
