# Generated by Django 4.2.3 on 2023-07-25 08:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0040_repository_organization'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repository',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.organization', verbose_name='organization'),
        ),
    ]
