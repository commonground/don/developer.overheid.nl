# Generated by Django 4.2.3 on 2023-10-05 10:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0045_validator"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="apivalidatorreport",
            options={},
        ),
    ]
