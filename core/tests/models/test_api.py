from django.test import TestCase

from core.models.api import API, APIValidatorReport
from core.models.organization import Organization
from core.models.validator import ValidatorReport

from .utils import create_ruleset


class DeleteTests(TestCase):
    def test_api_validator_report(self):
        ruleset = create_ruleset()
        api = API.objects.create(organization=Organization.objects.create(ooid=1))

        report = APIValidatorReport.objects.create(report=ValidatorReport.objects.create(ruleset=ruleset), api=api)
        report.delete()

        self.assertEqual(API.objects.count(), 1)
        self.assertEqual(ValidatorReport.objects.count(), 0)
        self.assertEqual(APIValidatorReport.objects.count(), 0)

    def test_base_validator_report(self):
        ruleset = create_ruleset()
        api = API.objects.create(organization=Organization.objects.create(ooid=1))

        base_report = ValidatorReport.objects.create(ruleset=ruleset)
        APIValidatorReport.objects.create(report=base_report, api=api)

        base_report.delete()

        self.assertEqual(API.objects.count(), 1)
        self.assertEqual(ValidatorReport.objects.count(), 0)
        self.assertEqual(APIValidatorReport.objects.count(), 0)


class LatestValidatorReportsTests(TestCase):
    def test_latest_reports(self):
        ruleset = create_ruleset()
        api = API.objects.create(organization=Organization.objects.create(ooid=1))

        APIValidatorReport.objects.create(report=ValidatorReport.objects.create(ruleset=ruleset), api=api)
        api_report = APIValidatorReport.objects.create(report=ValidatorReport.objects.create(ruleset=ruleset), api=api)

        reports = api.latest_validator_reports
        self.assertEqual(reports.count(), 1)

        report = reports.first()
        self.assertEqual(report.report.pk, api_report.report.pk)
