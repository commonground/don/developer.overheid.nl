from datetime import datetime, timezone

from django.test import TestCase
from django.db.models.deletion import ProtectedError

from core.models.api import API
from core.models.organization import Organization
from core.models.repository import Repository


class DeleteTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.obj: Organization = Organization.objects.create(ooid=42, name="Test Organization")

    def test_used_by_api(self):
        API.objects.create(api_id="test-api", organization=self.obj)

        with self.assertRaises(ProtectedError):
            self.obj.delete()

        deleted, _ = Organization.objects.delete_if_not_referenced(ooid=self.obj.ooid)
        self.assertEqual(deleted, 0)

    def test_used_by_repository(self):
        last = datetime(2023, 9, 26, 13, 31, 48, tzinfo=timezone.utc)
        Repository.objects.create(organization=self.obj, last_change=last, last_fetched_at=last)

        with self.assertRaises(ProtectedError):
            self.obj.delete()

        deleted, _ = Organization.objects.delete_if_not_referenced(ooid=self.obj.ooid)
        self.assertEqual(deleted, 0)

    def test_not_referenced(self):
        deleted, _ = Organization.objects.delete_if_not_referenced(ooid=self.obj.ooid)
        self.assertEqual(deleted, 1)
