from core.models.validator import Validator, ValidatorRuleset


def create_ruleset(validator_id: str = "test-validator") -> ValidatorRuleset:
    ruleset = ValidatorRuleset.objects.create(
        validator=Validator.objects.create(validator_id=validator_id),
        ruleset_id="test-ruleset", title="Test ruleset", description="A ruleset for testing", version="1.0.0")

    ruleset.rules.create(rule_id="test-rule", title="Test rule", description="Test description")
    ruleset.rules.create(rule_id="test-rule-2", title="Test rule 2", description="Test description 2")

    return ruleset
