from unittest.mock import MagicMock, patch

from django.test import SimpleTestCase
from requests import Session

from core.http import create_http_client, default_client


@patch.object(Session, "request")
class HttpClientTests(SimpleTestCase):
    def test_default_client(self, request: MagicMock):
        default_client.get("https://example.com")
        request.assert_called_once_with("GET", 'https://example.com', timeout=5, allow_redirects=True)

    def test_base_url(self, request: MagicMock):
        client = create_http_client("https://example.com/path")
        client.get("/file.html")
        request.assert_called_once_with("GET", 'https://example.com/path/file.html', timeout=5, allow_redirects=True)
