from django.test import SimpleTestCase

from core.utils import slugify_max


class TestUtils(SimpleTestCase):
    def test_slugify(self):
        value = "Lorem ipsum dolor sit amet"

        self.assertEqual("lorem-ipsum-dolor-sit-amet", slugify_max(value))
        self.assertEqual("lorem-ipsum-dolor-sit-amet", slugify_max(value, 26))
        self.assertEqual("lorem-ipsum-dolor-sit", slugify_max(value, 25))
        self.assertEqual("lore", slugify_max(value, 4))
