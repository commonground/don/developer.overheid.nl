from datetime import datetime, timezone

from django.test import TestCase

from core.models.repository import Repository
from core.models.organization import Organization
from core.validators.repository import RepositoryValidator


class RepositoryValidatorTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.validator = RepositoryValidator(validator_id="test")
        cls.validator.load_metadata()

        cls.organization = Organization.objects.create(ooid=1)

    def test_metadata(self):
        model = self.validator.model

        self.assertEqual(model.rulesets.count(), 1)
        self.assertEqual(model.rulesets.first().rules.count(), 2)

    def test_all_rules_passed(self):
        Repository.objects.create(
            id=1,
            organization=self.organization,
            description="non empty description",
            readme_url="http://example.com/README.md",
            last_change=datetime(2023, 7, 11, 12, 34, 56, tzinfo=timezone.utc),
            last_fetched_at=datetime(2023, 7, 13, 12, 34, 56, tzinfo=timezone.utc),
        )

        report = self.validator.validate_ruleset("metadata", 1)

        self.assertEqual(report.rule_total_count, 2)
        self.assertEqual(report.rule_passed_count, 2)

    def test_description_rule(self):
        Repository.objects.create(
            id=1,
            organization=self.organization,
            description="",
            last_change=datetime(2023, 7, 11, 12, 34, 56, tzinfo=timezone.utc),
            last_fetched_at=datetime(2023, 7, 13, 12, 34, 56, tzinfo=timezone.utc),
        )

        report = self.validator.validate_ruleset("metadata", 1)
        result = report.results.get(rule__rule_id="beschrijving")
        self.assertFalse(result.passed)

    def test_readme_rule(self):
        Repository.objects.create(
            id=1,
            organization=self.organization,
            readme_url="",
            last_change=datetime(2023, 7, 11, 12, 34, 56, tzinfo=timezone.utc),
            last_fetched_at=datetime(2023, 7, 13, 12, 34, 56, tzinfo=timezone.utc),
        )

        report = self.validator.validate_ruleset("metadata", 1)
        result = report.results.get(rule__rule_id="readme")
        self.assertFalse(result.passed)
