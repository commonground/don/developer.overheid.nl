from django.utils.text import slugify
from ruamel.yaml import YAML
from ruamel.yaml.scalarstring import LiteralScalarString
from ruamel.yaml.scalarstring import DoubleQuotedScalarString
from ruamel.yaml.compat import StringIO

# Taken from https://yaml.readthedocs.io/en/latest/example/#output-of-dump-as-a-string


class YAMLRuamel(YAML):
    def dump_string(self, data, **kw):
        """dump yaml output to a string"""
        stream = StringIO()
        YAML.dump(self, data, stream, **kw)
        return stream.getvalue().rstrip()


yaml_ruamel = YAMLRuamel()
yaml_ruamel.indent(mapping=2, sequence=4, offset=2)
yaml_ruamel.preserve_quotes = True


def yaml_output_newlines(string: str):
    return LiteralScalarString(string)


def yaml_output_doublequotes(string: str):
    return DoubleQuotedScalarString(string)

# Taken from https://code.djangoproject.com/ticket/15307#comment:5


def slugify_max(value, max_length=50):
    slug = slugify(value)
    if len(slug) <= max_length:
        return slug

    trimmed_slug = slug[:max_length].rsplit("-", 1)[0]
    if len(trimmed_slug) <= max_length:
        return trimmed_slug

    # First word is > max_length chars, so we have to break it
    return slug[:max_length]
