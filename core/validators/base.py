from collections.abc import Iterable
import json
from pathlib import Path
import os
import shlex
import subprocess
from typing import NamedTuple

from django.core.exceptions import ImproperlyConfigured
from django.db import transaction
from django.utils.functional import cached_property

from core.models.validator import (
    Validator as ValidatorModel, ValidatorRuleset, ValidatorRule, ValidatorReport, ValidatorResult)


class InvalidValidatorError(ImproperlyConfigured):
    pass


class UndefinedRulesetError(ImproperlyConfigured):
    pass


class ValidatorExecutionError(RuntimeError):
    pass


class ValidatorRegistry:
    def __init__(self, settings: dict):
        self._settings = settings
        self._validators = {}

    def __getitem__(self, validator_id: str):
        try:
            return self._validators[validator_id]
        except KeyError:
            try:
                cls = self._settings[validator_id]
            except KeyError:
                raise InvalidValidatorError(f"Validator with ID '{validator_id}' does not exist")  # noqa: pylint(raise-missing-from)

            validator = self._create_validator(cls, validator_id)
            self._validators[validator_id] = validator

            return validator

    def _create_validator(self, cls, validator_id: str) -> "Validator":
        validator: Validator = cls(validator_id)

        validator.check()
        validator.load_metadata()

        return validator


class Validator:
    class ValidatorMetadata(NamedTuple):
        name: str
        description: str
        version: str
        documentation_url: str | None = None

    class RulesetMetadata(NamedTuple):
        ruleset_id: str
        title: str
        description: str
        version: str
        documentation_url: str | None = None

    class RuleMetadata(NamedTuple):
        rule_id: str
        title: str
        description: str
        documentation_url: str | None = None

    def __init__(self, validator_id: str):
        self._id = validator_id

    @cached_property
    def model(self) -> ValidatorModel:
        obj, _ = ValidatorModel.objects.get_or_create(validator_id=self._id)
        return obj

    def check(self):
        pass

    @transaction.atomic
    def load_metadata(self):
        for key, value in self.get_validator_metadata()._asdict().items():
            setattr(self.model, key, value)
        self.model.save()

        ruleset_pks = []
        for ruleset, rules in self.get_rulesets_metadata():
            values = {
                "title": ruleset.title,
                "description": ruleset.description,
                "version": ruleset.version,
                "documentation_url": ruleset.documentation_url,
            }
            ruleset_obj, _ = self.model.rulesets.update_or_create(ruleset_id=ruleset.ruleset_id, defaults=values)
            ruleset_pks.append(ruleset_obj.pk)

            rule_pks = []
            for rule in rules:
                values = {
                    "title": rule.title,
                    "description": rule.description,
                    "documentation_url": rule.documentation_url
                }
                rule_obj, _ = ruleset_obj.rules.update_or_create(rule_id=rule.rule_id, defaults=values)
                rule_pks.append(rule_obj.pk)

            ruleset_obj.rules.exclude(pk__in=rule_pks).delete()

        self.model.rulesets.exclude(pk__in=ruleset_pks).delete()

    def get_validator_metadata(self) -> ValidatorMetadata:
        raise NotImplementedError()

    def get_rulesets_metadata(self) -> Iterable[tuple[RulesetMetadata, Iterable[RuleMetadata]]]:
        raise NotImplementedError()

    def validate_ruleset(self, ruleset_id: str, uri: str) -> ValidatorReport:
        try:
            ruleset = self.model.rulesets.get(ruleset_id=ruleset_id)
        except ValidatorRuleset.DoesNotExist as e:
            raise UndefinedRulesetError(f"Ruleset '{ruleset_id}' not defined for validator '{self._id}'") from e

        results = self.run(ruleset, uri)

        report = ValidatorReport.objects.create_with_results(results, uri=uri, ruleset=ruleset)

        return report


    def run(self, ruleset: ValidatorRuleset, uri: str) -> Iterable[ValidatorResult]:
        raise NotImplementedError()


class CLIValidator(Validator):
    name: str = None
    description: str = None
    documentation_url = ""

    def __init__(self, validator_id: str, path: str):
        super().__init__(validator_id)

        self._path = path

    def check(self):
        if not Path(self._path).is_file():
            raise ImproperlyConfigured(f"Path to validator does not exist: {self._path}")

        if not os.access(self._path, os.X_OK):
            raise InvalidValidatorError(f"Validator is not executable: {self._path}")

    def get_validator_metadata(self):
        output: list[dict] = self._execute("version")
        return Validator.ValidatorMetadata(self.name, self.description, output["version"], self.documentation_url)

    def get_rulesets_metadata(self):
        def _rules(rules):
            for rule in rules:
                yield Validator.RuleMetadata(**rule)

        output: list[dict] = self._execute("rulesets")

        for ruleset in output:
            rules = _rules(ruleset.pop("rules"))

            yield Validator.RulesetMetadata(**ruleset), rules

    def run(self, ruleset: ValidatorRuleset, uri: str) -> Iterable[ValidatorResult]:
        output: list[dict] = self._execute("validate", ruleset.ruleset_id, uri)

        results = []
        for output_result in output:
            try:
                rule_id = output_result["rule_id"]
                rule = ruleset.rules.get(rule_id=rule_id)
            except ValidatorRule.DoesNotExist as error:
                raise ValidatorExecutionError(f"Output contains unexpected rule: {rule_id}") from error

            result = ValidatorResult(
                passed=output_result["passed"],
                message=output_result["message"],
                details=output_result["details"],
                rule=rule)
            results.append(result)

        return results

    def _execute(self, command, *args):
        cmd = (self._path, "--format=json", command, *map(shlex.quote, args))

        try:
            output = subprocess.check_output(cmd, shell=False, text=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as error:
            raise ValidatorExecutionError(f"Process failed: {error.output}") from error

        try:
            return json.loads(output)
        except json.JSONDecodeError as error:
            raise ValidatorExecutionError(f"Decoding JSON output: {error}") from error
