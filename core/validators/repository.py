from collections.abc import Iterable

from core.models.repository import Repository
from core.models.validator import ValidatorRuleset, ValidatorResult, ValidatorRule

from .base import Validator


class RepositoryValidator(Validator):
    name = "Repository validator"
    description = "Valideert de herbruikbaarheidskwaliteit van een repository."
    version = "0.1.0"

    @staticmethod
    def _validate_description(repository: Repository, rule: ValidatorRule):
        passed = bool(repository.description)
        message = "Geen beschrijving ingevuld voor deze repository" if not passed else ""
        return ValidatorResult(passed=passed, message=message, rule=rule)

    @staticmethod
    def _validate_readme(repository: Repository, rule: ValidatorRule):
        passed = bool(repository.readme_url)
        message = "Deze repository heeft geen README bestand" if not passed else ""
        return ValidatorResult(passed=passed, message=message, rule=rule)

    _rules = (
        (
            Validator.RuleMetadata(
                "beschrijving",
                "Beschrijving ingevuld",
                "Een beschrijving is nodig om inzicht te krijgen in een repository.",
            ),
            _validate_description
        ),
        (
            Validator.RuleMetadata(
                "readme",
                "README beschikbaar",
                (
                    "Een README tekst bestand met de doelstellingen en gebruiksinstructies van een repository "
                    "is nodig om het gebruik ervan te faciliteren."
                ),
            ),
            _validate_readme
        ),
    )

    def get_validator_metadata(self):
        return Validator.ValidatorMetadata(self.name, self.description, self.version)

    def get_rulesets_metadata(self):
        title = "Repository-metagegevens"
        description = (
            "Om de bruikbaarheid van een repository te verhogen is het belangrijk dat bepaalde "
            "metagegevens zijn ingevuld."
        )

        def rules():
            for rule, _ in self._rules:
                yield rule

        yield Validator.RulesetMetadata("metadata", title, description, "0.1.0"), rules()

    def run(self, ruleset: ValidatorRuleset, uri: str) -> Iterable[ValidatorResult]:
        repository = Repository.objects.get(id=uri)

        for (rule_id, *_), test in self._rules:
            rule = ruleset.rules.get(rule_id=rule_id)
            result = test(repository, rule)
            yield result
