from . import api_version


class APIVersionMixin:
    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        response["API-Version"] = api_version
        return response
