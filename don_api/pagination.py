from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param


class LinkHeaderPagination(PageNumberPagination):
    def get_paginated_response_schema(self, schema):
        return schema

    def get_paginated_response(self, data):
        return Response(data, headers=self.get_headers())

    def get_headers(self):
        if len(self.page) == 1:
            return None

        links = []
        link_template = "<{}>; rel=\"{}\""

        if url := self.get_previous_link():
            links.append(link_template.format(url, "prev"))

        if url := self.get_next_link():
            links.append(link_template.format(url, "next"))

        if url := self.get_first_link():
            links.append(link_template.format(url, "first"))

        if url := self.get_last_link():
            links.append(link_template.format(url, "last"))

        return {"Link": ", ".join(links)}

    def get_first_link(self):
        if not self.page.has_previous():
            return None

        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, 1)

    def get_last_link(self):
        if not self.page.has_next():
            return None

        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, self.page.paginator.num_pages)
