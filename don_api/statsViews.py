from datetime import timedelta, datetime
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from .mixins import APIVersionMixin
from django.db import connection
from collections import defaultdict

def get_api_totals(start_date: datetime, end_date: datetime, api_authentication: list, api_types: list, organization_id: str = None):

    query = """
    SELECT COUNT(cv.uri), DATE(cv.generated_at) 
    FROM core_validatorreport cv
    INNER JOIN core_api ca ON cv.uri = ca.api_id
    WHERE cv.generated_at >= %s AND cv.generated_at <= %s
    AND ca.api_authentication = ANY(%s)
    AND ca.api_type = ANY(%s)
    """
    params = [start_date, end_date, api_authentication, api_types]

    # %s contains all params, but its place in the query determines which param is injected 
    # eg: the first time %s is used, it will inject `start_date`
    # sql can be efficient but archaic
    if organization_id:
        query += " AND ca.organization_id = %s"
        params.append(organization_id)

    query += "GROUP BY DATE(cv.generated_at) ORDER BY DATE(cv.generated_at)"

    with connection.cursor() as cursor:
        cursor.execute(query, params)
        results = cursor.fetchall()

    return results

def map_count_by_months(results, start_date):

    dates_by_month = get_daterange_till_today(start_date)
    results_mapped_to_months = {
        "labels": dates_by_month,
        "data": []
    }
    for string_date in dates_by_month:
        date = datetime.strptime(string_date, "%Y-%m-%d").date()
        result_dates = [row[1] for row in results] 
        if date in result_dates:
            date_index = result_dates.index(date)
            value = results[date_index][0]
            results_mapped_to_months["data"].append(value)
        else:
            results_mapped_to_months["data"].append(0)

    return results_mapped_to_months
    

def map_count_by_months_with_dates(results, result_dates, start_date):

    dates_by_month = get_daterange_till_today(start_date)
    results_mapped_to_months = []
    for string_date in dates_by_month:
        date = datetime.strptime(string_date, "%Y-%m-%d").date()
        if date in result_dates:
            date_index = result_dates.index(date)
            results_mapped_to_months.append(results[date_index])
        else:
            results_mapped_to_months.append(0)

    return results_mapped_to_months
    


class APIStatsViewSet(APIVersionMixin, ViewSet):

    def list(self, request, *args, **kwargs):

        start_date = getStartDate(request)
        end_date = datetime.now()
        api_authentication = getSecurityTypes(request)
        api_types = getApiTypes(request)
        organization_id = request.query_params.get("org")

        results = get_api_totals(start_date, end_date, api_authentication, api_types, organization_id)
        results_mapped_to_months = map_count_by_months(results, start_date)

        data = {
            "labels": results_mapped_to_months["labels"],
            "data": results_mapped_to_months["data"],
        }

        return Response(data)


def getStartDate(request) -> datetime:

    # Get the 'months' parameter from the query parameters
    months = request.query_params.get("months", 12)
    try:
        months = int(months)
    except ValueError:
        months = 12  # Default to 12 if the provided value is not an integer
    start_date = datetime.now() - timedelta(days=int(months) * 30)

    # Set the time component of start_date to the beginning of the day
    # to prevent date quirks when comparing dates
    start_date = start_date.replace(hour=0, minute=0, second=0, microsecond=0)
    
    return start_date

allowed_api_type_objects = [
    {
        "slug": "odata",
        "title": "OData",
    }, {
        "slug": "wms",
        "title": "WMS",
    }, {
        "slug": "graphql",
        "title": "GraphQL",
    }, {
        "slug": "rest_json",
        "title": "REST JSON",
    }, {
        "slug": "unknown",
        "title": "Unknown",
    }, {
        "slug": "rest_xml",
        "title": "REST XML",
    }, {
        "slug": "wfs",
        "title": "WFS",
    }
]

allowed_api_types = [api_type["slug"] for api_type in allowed_api_type_objects]

def getApiTypes(request):

    if request.query_params.get("api_type") is None:
        return allowed_api_types

    api_types = request.query_params.get("api_type").split("|")
    for api_type in api_types:
        if api_type not in allowed_api_types:
            return allowed_api_types

    return api_types

allowed_api_auth_types_objects = [
    {
        "slug": "unknown",
        "title": "Unknown",
    }, {
        "slug": "none",
        "title": "None",
    }, {
        "slug": "api_key",
        "title": "API key",
    }, {
        "slug": "oauth2",
        "title": "OAuth 2.0",
    },
]
allowed_api_auth_types = [api_sec_type["slug"] for api_sec_type in allowed_api_auth_types_objects]


def getSecurityTypes(request) -> list:

    if request.query_params.get("api_sec") is None:
        return allowed_api_auth_types

    api_auth_types = request.query_params.get("api_sec").split("|")
    for api_type in api_auth_types:
        if api_type not in allowed_api_auth_types:
            return allowed_api_auth_types

    return api_auth_types


def get_daterange_till_today(start_date: datetime) -> list[datetime]:
    today = datetime.now()
    date_range = []
    current_date = start_date
    while current_date <= today:
        date_range.append(current_date.strftime("%Y-%m-%d"))
        current_date += timedelta(days=30)
    return date_range


class OrgStatsViewSet(APIVersionMixin, ViewSet):

    def list(self, request, *args, **kwargs):

        start_date = getStartDate(request)
        end_date = datetime.now()
        api_authentication = getSecurityTypes(request)
        api_types = getApiTypes(request)

        organization_id = request.query_params.get("org")
        query = """
        SELECT COUNT(DISTINCT(ca.organization_id)), DATE(cv.generated_at) 
        FROM core_validatorreport cv
        INNER JOIN core_api ca ON cv.uri = ca.api_id
        WHERE cv.generated_at > %s AND cv.generated_at < %s
        AND ca.api_authentication = ANY(%s)
        AND ca.api_type = ANY(%s)
        """
        params = [start_date, end_date, api_authentication, api_types]

        if organization_id:
            query += " AND ca.organization_id = %s"
            params.append(organization_id)

        query += " GROUP BY DATE(cv.generated_at) ORDER BY DATE(cv.generated_at)"
        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()

        results_mapped_to_months = map_count_by_months(results, start_date)

        data = {
            "labels": results_mapped_to_months["labels"],
            "data": results_mapped_to_months["data"],
        }

        return Response(data)


core_rules = {
    5: "/core/http-methods",
    6: "/core/doc-openapi",
    7: "/core/uri-version",
    8: "/core/no-trailing-slash",
    9: "/core/publish-openapi",
    10: "/core/semver",
    11: "/core/version-header",
    16: "/core/http-methods",
    17: "/core/doc-openapi",
    18: "/core/uri-version",
    19: "/core/no-trailing-slash",
    20: "/core/publish-openapi",
    21: "/core/semver",
    22: "/core/version-header",
}

class AdrStatsViewSet(APIVersionMixin, ViewSet):

    def list(self, request, *args, **kwargs):
        rule_groups = defaultdict(list)
        for rule_id, label in core_rules.items():
            rule_groups[label].append(rule_id)

        start_date = getStartDate(request)
        end_date = datetime.now()
        api_authentication = getSecurityTypes(request)
        api_types = getApiTypes(request)
        organization_id = request.query_params.get("org")

        apisQuery = """
        SELECT COUNT(cv.uri), DATE(cv.generated_at) 
        FROM core_validatorreport cv
        INNER JOIN core_api ca ON cv.uri = ca.api_id
        WHERE cv.generated_at > %s AND cv.generated_at < %s
        AND ca.api_authentication = ANY(%s)
        AND ca.api_type = ANY(%s)
        """
        apisParams = [start_date, end_date, api_authentication, api_types]

        if organization_id:
            apisQuery += " AND ca.organization_id = %s"
            apisParams.append(organization_id)

        apisQuery += " GROUP BY DATE(cv.generated_at) ORDER BY DATE(cv.generated_at)"

        with connection.cursor() as cursor:
            cursor.execute(apisQuery, apisParams)
            apisResults = cursor.fetchall()

        apiResults = {}
        for result in apisResults:
            apiResults[result[1].strftime("%Y-%m-%d")] = result[0]

        query = """
        SELECT COUNT(valresult.rule_id), DATE(report.generated_at), valresult.rule_id
        FROM core_validatorresult valresult
        INNER JOIN core_validatorreport report ON valresult.report_id = report.id
        INNER JOIN core_apivalidatorreport api2report ON report.id = api2report.validator_report_id 
        INNER JOIN core_api api ON api2report.api_id = api.id
        WHERE valresult.passed = true AND (report.ruleset_id = 2 OR report.ruleset_id = 5)
        AND report.generated_at > %s AND report.generated_at < %s
        AND api.api_authentication = ANY(%s)
        AND api.api_type = ANY(%s)
        """
        params = [start_date, end_date, api_authentication, api_types]

        if organization_id:
            query += " AND api.organization_id = %s"
            params.append(organization_id)

        query += " GROUP BY DATE(report.generated_at), valresult.rule_id ORDER BY DATE(report.generated_at)"

        with connection.cursor() as cursor:
            cursor.execute(query, params)
            results = cursor.fetchall()

        dates = get_daterange_till_today(start_date)
        rules_with_percentages_per_date = defaultdict(lambda: defaultdict(float))

        for count, date, rule_id in results:
            date_str = date.strftime("%Y-%m-%d")
            if date_str in apiResults:
                percentage = round((count / apiResults[date_str]) * 100, 2)
                rule_label = core_rules.get(rule_id)
                if rule_label:
                    rules_with_percentages_per_date[rule_label][date_str] += percentage

        rule_results_by_date = {}
        for rule_label, rule_ids in rule_groups.items():
            result = {
                "title": rule_label,
                "data": [],
            }
            for date in dates:
                result["data"].append(rules_with_percentages_per_date[rule_label].get(date, 0))

            rule_results_by_date[rule_label] = result

        return Response({ "rule_data": rule_results_by_date, "labels": dates })


class ApiSecStatsViewSet(APIVersionMixin, ViewSet):

    def list(self, request, *args, **kwargs):

        start_date = getStartDate(request)
        end_date = datetime.now()
        api_authentication = getSecurityTypes(request)
        api_types = getApiTypes(request)
        organization_id = request.query_params.get("org")


        with connection.cursor() as cursor:

            query = """
            SELECT COUNT(cv.uri), DATE(cv.generated_at), ca.api_authentication 
            FROM core_validatorreport cv
            INNER JOIN core_api ca ON cv.uri = ca.api_id
            WHERE cv.generated_at > %s AND cv.generated_at < %s
            AND ca.api_authentication = ANY(%s)
            AND ca.api_type = ANY(%s)
            """
            params = [start_date, end_date, api_authentication, api_types]

            if organization_id:
                query += " AND ca.organization_id = %s"
                params.append(organization_id)

            query += " GROUP BY DATE(cv.generated_at), ca.api_authentication ORDER BY DATE(cv.generated_at)"

            cursor.execute(
                query,
                params,
            )
            results = cursor.fetchall()

            grouped_results = {
                api_type: {"labels": [], "data": []} for api_type in allowed_api_auth_types
            }

            for count, date, api_type in results:
                if api_type in grouped_results:
                    grouped_results[api_type]["labels"].append(date)
                    grouped_results[api_type]["data"].append(count)

        # Generate an array of labels using the datetime strings from start_date till end_date
        labels = get_daterange_till_today(start_date)

        response_data = {}
        for api_type in allowed_api_auth_types_objects:
            response_data[api_type["title"]] = map_count_by_months_with_dates(grouped_results[api_type["slug"]]["data"], grouped_results[api_type["slug"]]["labels"], start_date)

        response = {
            "labels": labels,
            "data": response_data,
        }

        return Response(response)


class ApiTypeStatsViewSet(APIVersionMixin, ViewSet):

    def list(self, request, *args, **kwargs):

        organization_id = request.query_params.get("org")
        start_date = getStartDate(request)
        end_date = datetime.now()
        api_authentication = getSecurityTypes(request)
        api_types = getApiTypes(request)

        with connection.cursor() as cursor:

            params = [start_date, end_date, api_authentication, api_types]

            query = """
            SELECT COUNT(cv.uri), DATE(cv.generated_at), ca.api_type 
            FROM core_validatorreport cv
            INNER JOIN core_api ca ON cv.uri = ca.api_id
            WHERE cv.generated_at > %s AND cv.generated_at < %s
            AND ca.api_authentication = ANY(%s)
            AND ca.api_type = ANY(%s)
            """

            if organization_id:
                query += " AND ca.organization_id = %s"
                params.append(organization_id)

            query += "GROUP BY DATE(cv.generated_at), ca.api_type ORDER BY DATE(cv.generated_at)"

            cursor.execute(
                query,
                params,
            )
            results = cursor.fetchall()

            grouped_results = {
                api_type: {"labels": [], "data": []} for api_type in allowed_api_types
            }

            query += " GROUP BY DATE(cv.generated_at), ca.api_authentication ORDER BY DATE(cv.generated_at)"

            for count, date, api_type in results:
                if api_type in grouped_results:
                    grouped_results[api_type]["labels"].append(date)
                    grouped_results[api_type]["data"].append(count)

        
        # Generate an array of labels using the datetime strings from start_date till end_date
        dates = get_daterange_till_today(start_date)
            
        api_type_results_with_zeroes = {}
        for api_type in allowed_api_type_objects:
            result = {
                "title": api_type["title"],
                "data": [],
            }
            api_type_results_with_zeroes[api_type["slug"]] = result
            api_type_dates = grouped_results[api_type["slug"]]["labels"]

            for stringDate in dates:
                
                date = datetime.strptime(stringDate, "%Y-%m-%d").date()

                if date in api_type_dates:
                    date_index = grouped_results[api_type["slug"]]["labels"].index(date)
                    the_found_date_value = grouped_results[api_type["slug"]]["data"][date_index]
                    api_type_results_with_zeroes[api_type["slug"]]["data"].append(the_found_date_value)
                else:
                    api_type_results_with_zeroes[api_type["slug"]]["data"].append(0)


        # MAKE SURE THAT IF WE ARE MISSING DATAPOINTS, WE ADD EMPTY VALUES (0)
        response = {
            "labels": dates,
            "data": api_type_results_with_zeroes
        }

        return Response(response)