import json

from rest_framework.test import APITestCase
from rest_framework.compat import SHORT_SEPARATORS

from core.models.api import API, Organization


class APITests(APITestCase):
    maxDiff = None

    def test_list(self):
        API.objects.create(
            api_id="api-1",
            service_name="API 1",
            description="Description 1",
            organization=Organization.objects.create(ooid=1, name="Organization A"),
            api_type=API.APIType.REST_JSON,
            api_authentication=API.APIAuthentication.API_KEY,
            contact_email="contact@example.com",
            contact_url="https://example.com",
            terms_government_only=True,
            terms_pay_per_use=True,
            terms_uptime_guarantee=42.42,
            terms_support_response_time=5,
        )
        API.objects.create(
            api_id="api-2",
            service_name="API 2",
            description="Description 2",
            organization=Organization.objects.create(ooid=42, name="Organization B"),
            api_type=API.APIType.GRPC,
            api_authentication=API.APIAuthentication.NONE,
            contact_phone="+31 (0)1 234 567 89",
            terms_uptime_guarantee=99,
        ).environments.create(
            name="production",
            api_url="https://api.example.com/",
            specification_url="https://api.example.com/openapi.json",
            documentation_url="https://example.com/docs",
        )

        response = self.client.get("/api/v0/apis")
        expected = [
            {
                "id": "api-1",
                "service_name": "API 1",
                "description": "Description 1",
                "organization": {
                    "ooid": 1,
                    "name": "Organization A",
                },
                "api_type": "rest_json",
                "api_authentication": "api_key",
                "environments": [],
                "contact": {
                    "email": "contact@example.com",
                    "phone": "",
                    "url": "https://example.com",
                },
                "is_reference_implementation": False,
                "terms_of_use": {
                    "government_only": True,
                    "pay_per_use": True,
                    "uptime_guarantee": 42.42,
                    "support_response_time": 5,
                },
            },
            {
                "id": "api-2",
                "service_name": "API 2",
                "description": "Description 2",
                "organization": {
                    "ooid": 42,
                    "name": "Organization B",
                },
                "api_type": "grpc",
                "api_authentication": "none",
                "environments": [
                    {
                        "name": "production",
                        "api_url": "https://api.example.com/",
                        "specification_url": "https://api.example.com/openapi.json",
                        "documentation_url": "https://example.com/docs",
                    },
                ],
                "contact": {
                    "email": "",
                    "phone": "+31 (0)1 234 567 89",
                    "url": "",
                },
                "is_reference_implementation": False,
                "terms_of_use": {
                    "government_only": None,
                    "pay_per_use": None,
                    "uptime_guarantee": 99.0,
                    "support_response_time": None,
                },
            },
        ]

        self.assertEqual(response.content.decode(), json.dumps(expected, separators=SHORT_SEPARATORS))
