from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter

from . import api_marjor_version
from .views import APIRootView, APIViewSet, OpenAPIView, RepositoryViewSet, OrganizationsViewSet
from .statsViews import APIStatsViewSet, OrgStatsViewSet, AdrStatsViewSet, ApiSecStatsViewSet, ApiTypeStatsViewSet

class Router(DefaultRouter):
    include_format_suffixes = False
    APIRootView = APIRootView


router = Router(trailing_slash=False)
router.register("/apis", APIViewSet)
router.register("/repositories", RepositoryViewSet)
router.register("/organizations", OrganizationsViewSet)
router.register("/stats/number-of-apis", APIStatsViewSet, basename="number-of-apis")
router.register("/stats/number-of-orgs", OrgStatsViewSet, basename="number-of-orgs")
router.register("/stats/api-design-rules", AdrStatsViewSet, basename="api-design-rules")
router.register("/stats/api-sec-data", ApiSecStatsViewSet, basename="api-sec-data")
router.register("/stats/api-type-data", ApiTypeStatsViewSet, basename="api-type-data")

urlpatterns = [
    path(f"v{api_marjor_version}", include(router.urls)),
    re_path(rf"v{api_marjor_version}/openapi\.(?P<format>json|yaml)", OpenAPIView.as_view(), name="openapi"),
]
