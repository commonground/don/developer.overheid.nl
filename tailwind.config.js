const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./web/templates/**/*html",
    "./web/templates/*html",
    "./assets/bundles/don-stats-app/**/*.js",
    "./assets/bundles/don-stats-app/components/*.js",
    "./web/templates/*html",
  ],
  theme: {
    extend: {},
    colors: {
      ...colors,
      'sky-blue': '#31B9FF',
    },
    container: {
      center: true,
      padding: '1rem',
      screens: {
        lg: '1200px', // Max width of 960px on large screens
        xl: '1200px', // Max width of 1140px on extra-large screens
      },
    },
  },
  plugins: [],
}

