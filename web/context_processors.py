from django.conf import settings


def default(_):
    return {
        "GOATCOUNTER_ENABLED": settings.GOATCOUNTER_ENABLED,
    }


def add_host_to_context(request):
    incoming_domain = request.get_host()
    return {
        "incoming_domain": incoming_domain,
        "is_apis": incoming_domain.startswith("apis."),
        "is_oss": incoming_domain.startswith("oss."),
    }
