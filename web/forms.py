import logging

from django import forms
from django.conf import settings
from django.core import validators
from django.core.cache import cache

from core import gitlab
from core.models.api import API
from core.oo import organizations_api_client
from core.utils import yaml_ruamel, yaml_output_newlines, yaml_output_doublequotes


logger = logging.getLogger("console")


class URLField(forms.URLField):
    default_validators = [validators.URLValidator(schemes=["https"])]


class OrganizationField(forms.ChoiceField):
    CACHE_KEY_CHOICES = "organisation_choices"
    CACHE_KEY_DICT = "organisation_dict"
    CACHE_TTL = 60 * 60

    def __init__(self, **kwargs):
        super().__init__(choices=self._organisation_choices, **kwargs)

    def _organisation_choices(self):
        return cache.get_or_set(self.CACHE_KEY_CHOICES, self._fetch_organisations, self.CACHE_TTL)

    def _fetch_organisations(self):
        organisations = organizations_api_client.list_organizations()
        return [(item["systeem_id"], item["naam"]) for item in organisations]

    def _organisations_dict(self):
        return dict((c[0], c[1]) for c in iter(self.choices))

    def get_organisation_name(self, ooid: int):
        organisations_dict = cache.get_or_set(self.CACHE_KEY_DICT, self._organisations_dict, self.CACHE_TTL)
        try:
            return organisations_dict[ooid]
        except KeyError as error:
            raise AttributeError(f"No organisation with ooid: {ooid}") from error

    def has_choices(self):
        return next(iter(self.choices), None) is not None


class ReferenceImplementationField(forms.ModelChoiceField):
    def __init__(self, *args, **kwargs):
        super().__init__(
            queryset=API.objects.filter(is_reference_implementation=True), to_field_name="api_id",
            *args, **kwargs)

    def label_from_instance(self, obj: API):
        return f"{obj.service_name} - {obj.organization.name}"


class AddAPIForm(forms.Form):
    service_name = forms.CharField(min_length=1, max_length=255)
    description = forms.CharField(min_length=1, widget=forms.Textarea())
    organization_ooid = OrganizationField(required=False)
    api_type = forms.ChoiceField(required=False, choices=API.APIType.choices)
    api_authentication = forms.ChoiceField(required=False, choices=API.APIAuthentication.choices)

    production_api_url = URLField(required=True)
    production_specification_url = URLField(required=False)
    production_documentation_url = URLField(required=False)

    acceptance_api_url = URLField(required=False)
    acceptance_specification_url = URLField(required=False)
    acceptance_documentation_url = URLField(required=False)

    demo_api_url = URLField(required=False)
    demo_specification_url = URLField(required=False)
    demo_documentation_url = URLField(required=False)

    contact_email_address = forms.EmailField(required=False)
    contact_phone_number = forms.CharField(required=False)
    contact_url = URLField(required=False)

    reference_implementation = ReferenceImplementationField(required=False, empty_label="")

    government_only = forms.BooleanField(required=False)
    pay_per_use = forms.BooleanField(required=False)
    uptime_guarantee = forms.DecimalField(
        min_value=1, max_value=100, max_digits=3, decimal_places=1, required=True, initial=99.5)
    support_response_time = forms.IntegerField(
        min_value=1, max_value=365, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.auto_id = "field_%s"

    def yaml_content(self):
        form_data = self.cleaned_data
        yaml_input_dict = {
            "service_name": form_data["service_name"],
            "description": form_data["description"],
            "organization": {"name": OrganizationField().get_organisation_name(int(form_data["organization_ooid"])),
                             "ooid": int(form_data["organization_ooid"])},
            "api_type": form_data["api_type"],
            "api_authentication": form_data["api_authentication"],
            "environments": [
                {
                    "name": "production",
                    "api_url": form_data["production_api_url"],
                    "specification_url": form_data["production_specification_url"],
                    "documentation_url": form_data["production_documentation_url"],
                },
                {
                    "name": "demo",
                    "api_url": form_data["demo_api_url"],
                    "specification_url": form_data["demo_specification_url"],
                    "documentation_url": form_data["demo_documentation_url"],
                },
                {
                    "name": "acceptance",
                    "api_url": form_data["acceptance_api_url"],
                    "specification_url": form_data["acceptance_specification_url"],
                    "documentation_url": form_data["acceptance_documentation_url"],
                }
            ],
            "contact": {"email": form_data["contact_email_address"],
                        "phone": form_data["contact_phone_number"],
                        "url": form_data["contact_url"]
                        },
            "is_reference_implementation": bool(form_data["reference_implementation"]),
            "terms_of_use": {"government_only": form_data["government_only"],
                             "pay_per_use": form_data["pay_per_use"],
                             "uptime_guarantee": float(form_data["uptime_guarantee"]),
                             "support_response_time": form_data["support_response_time"]
                             }
        }

        yaml_input_dict = self.clear_empty_values(yaml_input_dict)

        # format yaml strings
        yaml_input_dict["description"] = yaml_output_newlines(yaml_input_dict["description"])
        try:
            # double quotes are required by yamllint used in content repository
            yaml_input_dict["contact"]["phone"] = yaml_output_doublequotes(yaml_input_dict["contact"]["phone"])
        except KeyError:
            # no need to surround phone with double quotes if it does not exist
            pass

        return yaml_ruamel.dump_string(yaml_input_dict)

    def clear_empty_values(self, value):
        """removes empty values from new API yaml input"""
        if isinstance(value, dict):
            new_value = {}
            for key_inner, value_inner in value.items():
                value_inner = self.clear_empty_values(value_inner)
                # False is a valid value for API fields with a boolean type
                if value_inner or value_inner is False:
                    new_value[key_inner] = value_inner

            # if an (environment) dict only has a 'name' key, then it is considered empty because it does not have an
            # api_url, documentation_url, or specification_url
            if list(new_value.keys()) == ['name']:
                return {}

            return new_value

        if isinstance(value, list):
            new_value = []
            for value_inner in value:
                if value_inner := self.clear_empty_values(value_inner):
                    new_value.append(value_inner)

            return new_value

        return value

    def create_issue(self):
        title = f"Add a new API: {self.cleaned_data['service_name']}"
        content = f"```yaml\n{self.yaml_content()}\n```"

        if settings.DEBUG:
            logger.info("Would have created an issue with:\ntitle: %s\ncontent:\n%s", title, content)
            return

        gitlab.create_issue(title, content, "New API")


class AddRepositoryForm(forms.Form):
    organization_ooid = OrganizationField(required=False)
    organization_name = forms.CharField(required=False)
    account_url = URLField(widget=forms.TextInput(attrs={"placeholder": "https://gitlab.com/mijn-organisatie"}))

    def yaml_content(self):
        form_data = self.cleaned_data
        yaml_input_dict = {
            "organization": {"name": OrganizationField().get_organisation_name(int(form_data["organization_ooid"])),
                             "ooid": int(form_data["organization_ooid"])},
        }

        account_url = form_data["account_url"]
        if account_url.startswith("https://gitlab.com/"):
            yaml_input_dict["repositories"] = [{"gitlab": account_url.replace("https://gitlab.com/", "")}]
        elif account_url.startswith("https://github.com/"):
            yaml_input_dict["repositories"] = [{"github": account_url.replace("https://github.com/", "")}]
        else:
            yaml_input_dict["repositories"] = [account_url]

        return yaml_ruamel.dump_string(yaml_input_dict)

    def create_issue(self):
        title = "Add a new repository account"
        content = f"```yaml\n{self.yaml_content()}\n```"

        if settings.DEBUG:
            logger.info("Would have created an issue with:\ntitle: %s\ncontent:\n%s", title, content)
            return

        gitlab.create_issue(title, content, "New repository account")
