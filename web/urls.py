from django.conf import settings
from django.urls import path
from django.views import defaults as default_views
from django.views.generic.base import RedirectView
from django.urls import reverse_lazy

from .views.api import (
    APIListView, APIDetailView, APISpecificationView, APIAddView,
    APIAddSuccessView, APIValidatorReportsView, OpenApiSpecGenerator
)
from .views.generic import TemplateView
from .views.index import RootRedirectView
from .views.organization import OrganizationDetailView, OrganizationListView
from .views.repository import RepositoryListView, RepositoryAddView, RepositoryAddSuccessView, RepositoryDetailView
from .views.statistics import StatisticsView, APIStatisticsView

app_name = "web"


def redirect_view(view_url):
    """
    Helper view function specifically for the redirects below since they take
    a kwarg slug as an argument.
    """
    return RedirectView.as_view(
        url=reverse_lazy(view_url, kwargs={}),
        permanent=True)


urlpatterns = [
    path("", RootRedirectView.as_view(), name="index_redirect"),
    path("apis", APIListView.as_view(), name="api_list"),
    path("apis/toevoegen", APIAddView.as_view(), name="api_add"),
    path("apis/toevoegen/succes", APIAddSuccessView.as_view(), name="api_add_success"),
    path("apis/<slug:api_id>", APIDetailView.as_view(), name="api_detail"),
    path("apis/<slug:api_id>/score-details", APIValidatorReportsView.as_view(), name="api_validator_reports"),
    path("apis/<slug:api_id>/specificatie/<str:environment_name>", APISpecificationView.as_view(),
         name="api_specification"),
    path("organisaties", OrganizationListView.as_view(), name="organization_list"),
    path("organisaties/<slug:slug>", OrganizationDetailView.as_view(), name="organization_detail"),
    path("repositories", RepositoryListView.as_view(), name="repository_list"),
    path("repositories/<slug:slug>", RepositoryDetailView.as_view(), name="repository_detail"),
    path('repositorys', redirect_view('web:repository_list')),
    path("toevoegen/repository", RepositoryAddView.as_view(), name="repository_add"),
    path("toevoegen/repository/succes", RepositoryAddSuccessView.as_view(), name="repository_add_success"),
    path("statistieken", StatisticsView.as_view(), name="statistics"),
    path("api-statistieken", APIStatisticsView.as_view(), name="statistics"),
    path("tools/oas-generator", OpenApiSpecGenerator.as_view(), name="oas_generator"),
    # path("kennisplatform-apis", KennisplatformApisView.as_view(), name="kennisplatform-apis"),
    # path("kennisplatform-apis/intentieverklaring-api-strategie", IntentieverklaringView.as_view(), name="intentieverklaring-api-strategie"),
    path("contact", TemplateView.as_view(title="Contact", template_name="contact.html"), name="contact"),
    path("over", TemplateView.as_view(
        title="Over Developer Overheid",
        description="Eén centrale plek voor de developer die voor of met de overheid ontwikkelt",
        template_name="content/about.html",
    ), name="content_about"),
    path("privacy", TemplateView.as_view(title="Privacyverklaring", template_name="content/privacy.html"),
         name="content_privacy"),
    path(".well-known/security.txt", RedirectView.as_view(url="https://www.ncsc.nl/.well-known/security.txt")),
]

if settings.DEBUG:
    urlpatterns += [
        path("error/400", default_views.bad_request, {"exception": Exception("Bad request")}),
    path("error/404", default_views.page_not_found, {"exception": Exception("Page not Found")}),
        path("error/500", default_views.server_error),
    ]
