from .generic import TemplateView
from django.views import View
from django.shortcuts import redirect
from web.context_processors import add_host_to_context

class RootRedirectView(View):
    def get(self, request):
        host_context = add_host_to_context(request)

        if host_context["is_apis"]:
            return redirect("/apis", permanent=True)
        if host_context["is_oss"]:
            return redirect("/repositories", permanent=True)

        return redirect("/apis")


class IndexView(TemplateView):
    template_name = "index.html"
    description = (
        "Eén centrale plek voor de developer die voor of met de overheid ontwikkelt"
    )
    apis_limit = 10

    def get_breadcrumbs(self):
        return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["community_categories"] = [
            {
                "icon": "jetpack",
                "title": "Aan de slag",
                "link_text": "Meer handleidingen",
                "community_link": "https://community.developer.overheid.nl/c/kennisbank/aan-de-slag/14",
                "items": [
                    {
                        "title": "Ontwerp een API",
                        "summary": """Hier leggen we uit welke stappen je kunt doorlopen om snel en efficient een
                        nieuwe REST API te ontwikkelen. We gaan hierbij uit van een design first aanpak, dus we starten 
                        met het API design.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=171"
                    },
                    {
                        "title": "Start een Open Source project",
                        "summary": """Open Source werken heeft veel voordelen. Maar hoe ziet de codebase van een gezond Open Source project er uit? En waar moet je nog meer rekening mee houden? Antwoorden daarop krijg je in dit artikel.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=308"
                    }, {
                        "title": "Gebruik API's van de overheid",
                        "summary": "Overheidsorganisaties ontwikkelen veel API’s om allerlei data snel en efficient te delen met andere overheidsorganisaties en andere partijen die met de overheid werken. In deze post leer je hoe je zelf deze API’s kan gebruiken.",
                        "link": "https://community.developer.overheid.nl/docs?topic=309"
                    }, {
                        "title": "Git: hoe gebruik ik Git in een Open Source project?",
                        "summary": "Binnen Open Source projecten wordt Git veelal op een specifiek manier ingezet. Zo worden bijdragen vanuit de community toegelaten, of niet. ",
                        "link": "https://community.developer.overheid.nl/docs?topic=477",
                        "organization": "kadaster"
                    }, {
                        "title": "Samen werken aan web-componenten met NL Design System",
                        "summary": "NL Design System is een set van richtlijnen, componenten en tools.",
                        "link": "https://community.developer.overheid.nl/docs?topic=591",
                        "organization": "nl-design-system",
                        "tags": ["open-source", "front-end", "community", "nl-design-system", "design-system"]
                    }
                ],
            },
            {
                "icon": "clipboard-check",
                "title": "Richtlijnen",
                "link_text": "Meer richtlijnen",
                "community_link": "https://community.developer.overheid.nl/c/kennisbank/richtlijnen/18",
                "items": [
                    {
                        "title": "Standaard Digitoegankelijk",
                        "summary": """De standaard Digitoegankelijk is een lijst van succescriteria die ervoor zorgt dat websites en applicaties toegankelijk zijn voor alle burgers. """,
                        "link": "https://community.developer.overheid.nl/docs?topic=601",
                        "tags": ["digitale-toegankelijkheid", "forum-standaardisatie", "accessibility", "wcag"]
                    },
                    {
                        "title": "Publiccode.yml",
                        "summary": """Om een open source project inzichtelijk te maken is het belangrijk dat bepaalde metadata beschikbaar is. Hiervoor is de publiccode.yml standaard in het leven geroepen.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=311",
                        "tags": ["open-source", "publiccode.yml", "metadata"]
                    },
                    {
                        "title": "OpenAPI Specification (OAS)",
                        "summary": """De OpenAPI Specification (OAS) is een open standaard om REST API’s te 
                        beschrijven.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=310"
                    },
                    {
                        "title": "REST API Design Rules (ADR)",
                        "summary": """De REST API Design Rules (ADR) schrijven voor waar een REST API van de overheid 
                        aan moet voldoen. Deze regels zijn verplicht gesteld door Forum Standaardisatie, waardoor elke 
                        overheid hieraan moet voldoen.""",
                        "organization": "kpa",
                        "link": "https://community.developer.overheid.nl/docs?topic=183"
                    },
                    {
                        "title": "Security.txt",
                        "summary": """De security.txt standaard zorgt ervoor dat ethische hackers laagdrempelig kwetsbaarheden kunnen melden bij de eigenaar/ beheerder van een applicatie.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=359"
                    },
                    {
                        "title": "Standaard voor publieke code",
                        "summary": """De standaard voor publieke code is een gids die beoogt handvatten te geven bij het werken aan Open Source Software in de publieke sector.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=595",
                        "tags": ["open-source", "public-code", "documentatie", "handleiding"]
                    },
                ],
            },
            {
                "icon": "terminal-2",
                "title": "Tools",
                "link_text": "Meer tools",
                "community_link": "https://community.developer.overheid.nl/c/kennisbank/tools/17",
                "items": [
                    {
                        "title": "OpenKat - Kwetsbaarheden Analyse Tool",
                        "summary": """ OpenKAT is ontwikkeld om de (beveiligings-)status van informatiesystemen te monitoren, analyseren en op te slaan. """,
                        "organization": "vws",
                        "link": "https://community.developer.overheid.nl/docs?topic=482",
                        "tags": ["security", "monitoring"]
                    },
                    {
                        "title": "Beslisboom Open Standaarden",
                        "summary": """ Met deze beslisboom ontdekt je welke ICT-standaarden van de 'Pas toe of leg uit'-lijst relevant zijn voor jouw situatie.  """,
                        "organization": "forum-standaardisatie",
                        "link": "https://community.developer.overheid.nl/docs?topic=544",
                        "tags": ["development", "beslisboom", "open-standaarden"]
                    },
                    {
                        "title": "Quality Time - Software quality monitoring",
                        "summary": """ Quality-time is een open source tool die je kan helpen de kwaliteit van je maatwerksoftware te bewaken en technische schuld te beheren. """,
                        "organization": "ictu",
                        "link": "https://community.developer.overheid.nl/docs?topic=542",
                        "tags": ["quality", "software-development", "metrics", "software-maintenance"]
                    },
                    {
                        "title": "OpenAPI Specification Generator",
                        "summary": """De OAS Generator genereert een OAS boilerplate op basis van minimale configuratie. """,
                        "link": "https://community.developer.overheid.nl/docs?topic=189"
                    },
                    {
                        "title": "API Design Rules Validator",
                        "summary": """De API Design Rules (ADR) Validator is een command line interface die gebruikt kan worden om te valideren of een API zicht gedraagt conform de NL API Strategie.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=188"
                    },
                    {
                        "title": "API Design Rules Linter",
                        "summary": """De ADR Linter controleert of een OpenAPI Specificatie compliant is met de API Design Rules. De linter is gebaseerd op het Open Source project Spectral.""",
                        "link": "https://community.developer.overheid.nl/docs?topic=182"
                    },
                    {
                        "title": "FSC - Rego policy builder",
                        "summary": """ Federatieve Service Connectivity (FSC) is een standaard voor veilige digitale gegevensuitwisseling. """,
                        "organization": "digilab",
                        "link": "https://community.developer.overheid.nl/docs?topic=400"
                    },
                    {
                        "title": "Publiccode.yml Editor ",
                        "summary": """ Met deze web-editor kan je laagdrempelig een publiccode.yml bestand genereren. """,
                        "organization": "developer-italia",
                        "link": "https://community.developer.overheid.nl/docs?topic=190",
                        "tags": ["open-source", "publiccode.yml", "metadata"]
                    },
                ],
            },
            {
                "icon": "folders",
                "title": "Templates",
                "link_text": "Meer templates",
                "community_link": "https://community.developer.overheid.nl/c/kennisbank/tools/17",
                "items": [
                    {
                        "title": "Python Project Template",
                        "summary": """ Python Project Template voor het opzetten van een Python project. """,
                        "organization": "rijks_ict_gilde",
                        "link": "https://github.com/RijksICTGilde/python-project-template",
                        "tags": ["python", "template", ".editorconfig", "VS Code", "Poetry package manager"]
                    },
                    {
                        "title": "Publiccode.yml ",
                        "summary": """ Voorbeeld van een vooringevulde publiccode.yml. """,
                        "link": "https://gitlab.com/commonground/don/developer.overheid.nl/-/blob/main/publiccode.yml",
                        "tags": ["open-source", "publiccode.yml", "metadata", "example"]
                    },
                ],
            },
        ]

        return context
