
from .generic import TemplateView
from .mixins import Breadcrumb

from django.urls import reverse_lazy


class KennisplatformApisView(TemplateView):
    title = "Kennisplatform API's"
    template_name = "kennisplatform-apis.html"
    description = "Het Kennisplatform API is een open platform waar overheden, marktpartijen, gebruikers en aanbieders samen werken aan een Nederlandse API strategie en alles wat daar verder bij komt kijken. Wil je meedoen?"

    breadcrumb = Breadcrumb(title=title, url=reverse_lazy("web:kennisplatform-apis"))


class IntentieverklaringView(TemplateView):
    title = "Intentieverklaring API Strategie"
    template_name = "intentieverklaring-api-strategie.html"
    description = "De intentieverklaring is opgesteld door het Kennisplatform API's in opdracht van het Ministerie van BZK. Het Kennisplatform API's is een samenwerkingsverband dat zich inzet voor het bevorderen van de uitwisseling van data en diensten binnen de overheid door middel van standaarden en API's."

    breadcrumb = Breadcrumb(title=title, url=reverse_lazy("web:intentieverklaring-api-strategie"))

    def get_breadcrumbs(self):
        return (
            KennisplatformApisView.breadcrumb,
            self.breadcrumb,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        img_path = "images/"
        context["organizations"] = [
            {
                "img": img_path + "FORUM_STANDAARDISATIE_LOGO_RGB-01_0.png",
                "name": "Larissa Zegveld",
                "role": "Voorzitter Forum Standaardisatie"
            }, {
                "img": img_path +"logius.png",
                "name": "Ing. P.J. den Held RI",
                "role": "Directeur Digitale Programma's"
            }, {
                "img": img_path + "SVB.png",
                "name": "Britt van den Berg",
                "role": "CIO"
            }, {
                "img": img_path + "Kadaster.png",
                "name": "Rob Agelink",
                "role": "CDO"
            }, {
                "img": img_path + "vng.png",
                "name": "Theo Peters",
                "role": "CTO"
            }, {
                "img": img_path + "RVIG.png",
                "name": "Michiel van der Veen",
                "role": "directeur Innovatie en Ontwikkeling"
            }, {
                "img": img_path + "KVK_1.png",
                "name": "Marije Hovestad",
                "role": "Directeur Dataverstrekking"
            }, {
                "img": img_path + "minvenj.png",
                "name": "Raymond Kers",
                "role": ""
            }, {
                "img": img_path + "Geonovum-ruimte.png",
                "name": "Rob van de Velde",
                "role": "Directeur"
            }
        ]
        return context
