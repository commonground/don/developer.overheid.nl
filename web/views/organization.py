from django.core.paginator import Page
from django.db.models import Count
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView

from core.models.organization import Organization
from core.models.repository import get_programming_languages_count

from web.context_processors import add_host_to_context
from web.views.generic import BaseView, ListView

from web.views.mixins import Breadcrumb


class OrganizationListView(ListView, BaseView):
    template_name = "organization_list.html"
    model = Organization
    context_object_name = "organizations_list"
    title = "Organisaties"
    description = "Overzicht van Nederlandse overheidsorganisaties met open API's of open source repositories."

    paginate_by = 10
    page_kwarg = "pagina"
    pages_on_each_side = 1
    pages_on_ends = 1

    breadcrumb = Breadcrumb(title=title, url=reverse_lazy("web:organization_list"))

    def get_queryset(self):
        context = add_host_to_context(self.request)
        is_apis = context["is_apis"]
        is_oss = context["is_oss"]

        queryset = Organization.objects.annotate(
            repository_count=Count("repository", distinct=True),
            api_count=Count("api", distinct=True),
        )

        if is_apis:
            return queryset.filter(api_count__gt=0).order_by("name")
        if is_oss:
            return queryset.filter(repository_count__gt=0).order_by("name")
        return queryset.filter(api_count__gt=0, repository_count__gt=0).order_by("name")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        page: Page = context["page_obj"]
        context["page_range"] = page.paginator.get_elided_page_range(
            page.number, on_each_side=self.pages_on_each_side, on_ends=self.pages_on_ends)
        context["page_kwarg"] = self.page_kwarg

        return context

    def get_breadcrumbs(self):
        return [
            self.breadcrumb
        ]


class OrganizationDetailView(DetailView, BaseView):
    template_name = "organization_detail.html"
    limit = 4
    model = Organization
    object: Organization

    queryset = (
        Organization.objects.prefetch_related(
            "repository_set__repositoryprogramminglanguage_set__programming_language",
        )
    )

    def get_title(self):
        return self.object.name

    def get_description(self):
        return self.object.description or ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = self.object
        repositories = organization.repository_set.all()

        repository_programming_languages = []
        for repository in repositories:
            repository_programming_languages.extend(repository.repositoryprogramminglanguage_set.all())

        context["repository_list"] = organization.repository_set.order_by("-last_change").all()[:self.limit]
        context["api_list"] = organization.api_set.order_by("service_name").all()[:self.limit]
        context["repository_list_all_count"] = organization.repository_set.count()
        context["api_list_all_count"] = organization.api_set.count()
        context["limit"] = self.limit
        context["top_programming_languages"] = get_programming_languages_count(repository_programming_languages)[:10]

        return context

    def get_breadcrumbs(self):
        return [
            OrganizationListView.breadcrumb,
            Breadcrumb(self.get_title(), reverse(
                "web:organization_detail", kwargs={"slug": self.object.slug}))
        ]
