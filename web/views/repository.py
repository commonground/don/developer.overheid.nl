from django.db.models import F
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, DetailView

from core.models.repository import Repository

from .generic import FacetListView, FacetFilter, BaseView, TemplateView
from .mixins import Breadcrumb

from ..forms import AddRepositoryForm


class RepositoryListView(FacetListView):
    template_name = "repository_list.html"
    partial_template_name = "partials/repository_list_results.html"
    title = "Zoek repositories"
    description = "Overzicht van open source projecten van Nederlandse overheidsorganisaties."
    breadcrumb = Breadcrumb(title, reverse_lazy('web:repository_list'))

    queryset = (
        Repository.objects.prefetch_related(
            "programming_languages",
            "repositoryprogramminglanguage_set",
            "related_apis",
            "topics"
        )
        .order_by(F("last_change").desc(nulls_last=True))
    )
    text_search_label = "Vind repositories"
    text_search_fields = ("owner_name", "name", "description")
    facets_filters = {
        "organisatie": FacetFilter(
            label="Organisatie", field_name="organization", choice_label_field="organization__name"),
        "technologie": FacetFilter(
            label="Technologie", field_name="programming_languages", choice_label_field="programming_languages__name"),
        "gearchiveerd": FacetFilter(
            label="Gearchiveerde repositories",
            field_name="archived",
            choice_label_field="archived",
            choices={True: "Toon alleen gearchiveerde"},
        ),
    }


class RepositoryDetailView(DetailView, BaseView):
    template_name = "repository_detail.html"
    limit = 4
    model = Repository
    object: Repository

    def get_title(self):
        return self.object.name

    def get_description(self):
        return self.object.description or ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        repository = self.object

        url_splitted = repository.owner_name.split("/")
        urls_splitted_absolute = []

        index = 0
        while index < len(url_splitted):
            joined = '/'.join(url_splitted[:index + 1])
            urls_splitted_absolute.append(joined)
            index += 1

        context["urls_splitted"] = url_splitted
        context["urls_splitted_absolute"] = urls_splitted_absolute

        if repository.source == Repository.Source.GITHUB:
            context["owner_name"] = repository.owner_name
            context["stars_url"] = f"{repository.url}/stargazers" if repository.stars is not None else None
            context["forks_url"] = f"{repository.url}/forks"
            context["issues_url"] = f"{repository.url}/issues" if repository.issue_open_count is not None else None
            context["merge_requests_url"] = f"{repository.url}/pulls" if repository.merge_request_open_count is not None else None
        elif repository.source == Repository.Source.GITLAB:
            context["owner_name"] = " / ".join(repository.owner_name.split("/"))
            context["stars_url"] = f"{repository.url}/-/starrers" if repository.stars is not None else None
            context["forks_url"] = f"{repository.url}/-/forks"
            context["issues_url"] = f"{repository.url}/-/issues" if repository.issue_open_count is not None else None
            context["merge_requests_url"] = f"{repository.url}/-/merge_requests" \
                if repository.merge_request_open_count is not None else None
        else:
            context["owner_name"] = repository.owner_name
            context["stars_url"] = None
            context["forks_url"] = None
            context["issues_url"] = None
            context["merge_requests_url"] = None

        return context

    def get_breadcrumbs(self):
        return [
            RepositoryListView.breadcrumb,
            Breadcrumb(self.get_title(), reverse(
                "web:repository_detail", kwargs={"slug": self.object.slug}))
        ]


class RepositoryAddView(FormView, BaseView):
    template_name = "repository_add.html"
    title = "Repository laten toevoegen"
    form_class = AddRepositoryForm
    success_url = reverse_lazy("web:repository_add_success")
    breadcrumb = Breadcrumb("toevoegen", reverse_lazy("web:repository_add"))

    def form_valid(self, form: AddRepositoryForm):
        form.create_issue()
        return super().form_valid(form)

    def get_breadcrumbs(self):
        return [
            RepositoryListView.breadcrumb,
            self.breadcrumb
        ]


class RepositoryAddSuccessView(TemplateView):
    title = "Account geregistreerd"
    template_name = "repository_add_success.html"
    breadcrumb = Breadcrumb("succes", reverse_lazy("web:repository_add_success"))

    def get_breadcrumbs(self):
        return [
            RepositoryListView.breadcrumb,
            RepositoryAddView.breadcrumb,
            self.breadcrumb
        ]
