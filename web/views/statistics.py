from core.models.repository import RepositoryProgrammingLanguage, get_programming_languages_count
from .generic import TemplateView


def get_top_programming_languages():

    repository_programming_languages = RepositoryProgrammingLanguage.objects.select_related(
        "programming_language",
    ).all()

    return get_programming_languages_count(repository_programming_languages)[:40]


class StatisticsView(TemplateView):
    template_name = "statistics.html"
    title = "Statistieken"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["top_programming_languages"] = get_top_programming_languages()

        return context


class APIStatisticsView(TemplateView):
    template_name = "api-statistics.html"
    title = "Statistieken"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context
